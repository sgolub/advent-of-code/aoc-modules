from collections import defaultdict

from aoc import AdventOfCode



class AOCYear2021Day3(AdventOfCode):
    year = 2021
    day = 3

    def parse_data(self, data):
        return [[int(b) for b in l] for l in data]

    @staticmethod
    def _toggle_bit(n, k):
        return (n ^ (1 << (k-1)))

    @staticmethod
    def _get_bit_counts(data):
        bits = [defaultdict(int) for i in range(len(data[0]))]
        for line in data:
            for i, b in enumerate(line):
                bits[i][b] += 1
        return bits

    async def solution_1(self):
        mc = ""
        data = self.data
        bits = self._get_bit_counts(data)
        for bit in bits:
            mc += str(max(bit.items(), key=lambda x: x[1])[0])
        gamma = int(mc, base=2)
        epsilon = gamma
        for i in range(len(mc)):
            epsilon = self._toggle_bit(epsilon, i+1)
        return gamma * epsilon

    @classmethod
    def _filter_commons(cls, data, method, keep_equal):
        data = data.copy()
        i = 0
        while len(data) > 1:
            bits = cls._get_bit_counts(data)
            if len(set(bits[i].values())) == 1:
                keep_value = keep_equal
            else:
                keep_value = method(bits[i].items(), key=lambda x: x[1])[0]
            if len(data) > 1:
                data = [d for d in data if d[i] == keep_value]
            i += 1
        return data[0]

    async def solution_2(self):
        o2 = self.data
        co2 = self._filter_commons(o2, min, 0)
        o2 = self._filter_commons(o2, max, 1)
        return int(''.join(map(str, o2)), base=2) * int(''.join(map(str, co2)), base=2)
