import numpy as np

from aoc import AdventOfCode



reset_value = 7

class LanternFish:
    def __init__(self, int_timer):
        self._timer = int_timer

    def increment(self):
        if not self._timer:
            self._timer = reset_value - 1
            return LanternFish(reset_value + 1)
        self._timer -= 1


class AOCYear2021Day6(AdventOfCode):
    year = 2021
    day = 6

    def _calc_lanternfish(self, days):
        fish_tracking = np.bincount(self.data, minlength=reset_value+2)
        for _ in range(days):
            fish_tracking = np.roll(fish_tracking, -1)
            fish_tracking[reset_value-1] += fish_tracking[reset_value+1]
        return np.sum(fish_tracking)

    async def solution_1(self):
        return self._calc_lanternfish(80)

    async def solution_2(self):
        return self._calc_lanternfish(256)
