import numpy as np

from aoc import AdventOfCode


class AOCYear2021Day7(AdventOfCode):
    year = 2021
    day = 7

    def parse_data(self, data):
        return np.array(data)

    async def solution_1(self):
        data = self.data
        f_min = None
        for p in range(max(data)):
            fuel = np.sum(np.abs(data-p))
            if not f_min:
                f_min = fuel
            elif fuel < f_min:
                f_min = fuel
        return f_min

    async def solution_2(self):
        fuel_min = None
        crab_positions = self.data
        fuel_costs = np.arange(1, max(crab_positions))
        for pos in range(max(crab_positions)):
            steps = np.abs(crab_positions-pos)
            fuel = 0
            for step in steps:
                fuel += np.sum(fuel_costs[0:step])
            if not fuel_min:
                fuel_min = fuel
            elif fuel < fuel_min:
                fuel_min = fuel
        return fuel_min
