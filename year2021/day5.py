from collections import defaultdict
import math
import typing as t

import numpy as np
from aoc import AdventOfCode



class Point:
    def __init__(self, x, y):
        self.x = int(x)
        self.y = int(y)

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __hash__(self):
        return hash(str(self))

    def __str__(self):
        return f"{self.x},{self.y}"

    def __repr__(self):
        return f"<Point {self!s}>"


class Line:
    def __init__(self, p1: Point, p2: Point):
        self.start = p1
        self.end = p2

    def walk(self):
        p1 = self.start
        p2 = self.end
        if p1.x == p2.x:
            if p1.y < p2.y:
                l = p1.y
                h = p2.y
            else:
                l = p2.y
                h = p1.y
            return [Point(p1.x, y) for y in range(l, h + 1)]
        elif p1.y == p2.y:
            if p1.x < p2.x:
                l = p1.x
                h = p2.x
            else:
                l = p2.x
                h = p1.x
            return [Point(x, p1.y) for x in range(l, h + 1)]
        return []

    @property
    def slope(self):
        try:
            return (self.bottommost.y - self.topmost.y) / (self.rightmost.x - self.leftmost.x)
        except ZeroDivisionError:
            return None

    def point_on_line(self, point: Point) -> bool:
        if self.vertical and self.start.x == point.x:
            return self.topmost.y <= point.y <= self.bottommost.y
        elif self.horizontal and self.start.y == point.y:
            return self.leftmost.x <= point.x <= self.rightmost.x
        return False

    @property
    def horizontal(self) -> bool:
        return self.start.y == self.end.y and self.start.x != self.end.x

    @property
    def vertical(self) -> bool:
        return self.start.x == self.end.x and self.start.y != self.end.y

    @property
    def leftmost(self) -> Point:
        return self.start if self.start.x < self.end.x else self.end

    @property
    def rightmost(self) -> Point:
        return self.end if self.start.x < self.end.x else self.start

    @property
    def topmost(self) -> Point:
        return self.start if self.start.y < self.end.y else self.end

    def __len__(self):
        return int(math.sqrt((self.end.x - self.start.x)**2 + (self.end.y - self.start.y)**2))

    @property
    def bottommost(self) -> Point:
        return self.end if self.start.y < self.end.y else self.start

    def intersects(self, other: t.ForwardRef("Line")) -> t.List[Point]:
        slen = len(self)
        olen = len(other)
        if slen > olen:
            longer = self
            shorter = other
        else:
            longer = other
            shorter = self

        points = []
        for point in shorter.walk():
            if longer.point_on_line(point):
                points.append(point)
        return points


class AOCYear2021Day5(AdventOfCode):
    year = 2021
    day = 5

    def parse_data(self, data) -> t.List[Line]:
        return [[[int(i) for i in p.split(",")] for p in l.split(" -> ")] for l in data.splitlines()]

    @staticmethod
    def _calc_intersections(data, include_diags=False):
        a = np.zeros((1000, 1000))
        for (x1, y1), (x2, y2) in data:
            if (x1 == x2) or (y1 == y2):
                for x in range(min(x1, x2), max(x1, x2)+1):
                    for y in range(min(y1, y2), max(y1, y2)+1):
                        a[y, x] += 1
            elif include_diags and abs(x2-x1) == abs(y2-y1):
                dist = abs(x2-x1)
                for i in range(dist+1):
                    if (x1 > x2) and (y1 > y2):
                        a[y1-i, x1-i] += 1
                    elif (x1 < x2) and (y1 > y2):
                        a[y1-i, x1+i] += 1
                    elif (x1 > x2) and (y1 < y2):
                        a[y1+i, x1-i] += 1
                    elif (x1 < x2) and (y1 < y2):
                        a[y1+i, x1+i] += 1
        return np.sum(a >= 2)

    async def solution_1(self):
        return self._calc_intersections(self.data)

    async def solution_2(self):
        return self._calc_intersections(self.data, True)
