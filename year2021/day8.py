from aoc import AdventOfCode



output_signals = {
    'abcefg': 0,
    'cf': 1,
    'acdeg': 2,
    'acdfg': 3,
    'bcdf': 4,
    'abcdfg': 5,
    'abdefg': 6,
    'acf': 7,
    'abdefg': 8,
    'abcdfg': 9,
}

class AOCYear2021Day8(AdventOfCode):
    year = 2021
    day = 8

    def parse_data(self, data):
        return [
            {'input': d.split(" | ")[0].split(), 'output': d.split(" | ")[1].split()}
            for d in data.splitlines()
        ]

    async def solution_1(self):
        c = 0
        for d in self.data:
            for o in d['output']:
                if len(o) in [2,3,4,7]:
                    c += 1
        return c

    async def solution_2(self):
        raise NotImplementedError
