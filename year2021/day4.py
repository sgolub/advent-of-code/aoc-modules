import itertools

from aoc import AdventOfCode


class Square:
    def __init__(self, number):
        self.number = number
        self.covered = False

    def __str__(self):
        return f"[{self.number:2}]" if self.covered else f" {self.number:2} "

    def __repr__(self):
        return f"<Square: {self}>"

class Board:
    def __init__(self, data):
        self._board = [[Square(c) for c in r] for r in data]
        self._reference = {c.number: c for c in itertools.chain(*[r for r in self._board])}

    def cover(self, number):
        s = self._reference.get(number)
        if s is not None:
            s.covered = True
            return True
        return False

    crosses = [
        [[0] * 2, [1] * 2, [2] * 2, [3] * 2, [4] * 2],
        [[0, 4], [1, 3], [2, 2], [3, 1], [4, 0]],
    ]
    def is_winner(self):
        # check rows
        for r in self._board:
            if all(s.covered for s in r):
                return True

        # check columns
        for x in range(5):
            if all(self._board[y][x].covered for y in range(5)):
                return True

        #  # check diagonals
        #  for line in self.crosses:
        #      if all(self._board[y][x].covered for x, y in line):
        #          return True
        return False

    def score(self):
        return sum(v.number for k,v in self._reference.items() if not v.covered)

    def __str__(self):
        return "\n".join("".join([str(s) for s in r]) for r in self._board)


class Game:
    def __init__(self, boards, draws):
        self._boards = boards
        self._draws = draws

    def get_winner(self):
        for board in self._boards:
            if board.is_winner():
                return board

    def __iter__(self):
        return iter(self._boards)

    def __len__(self):
        return len(self._boards)

    def play_to_win(self):
        for draw in self._draws:
            for board in self:
                board.cover(draw)
                if board.is_winner():
                    return board.score() * draw

    def play_to_lose(self):
        ignored_boards = []
        for draw in self._draws:
            for i, board in enumerate(self):
                if i in ignored_boards:
                    continue
                board.cover(draw)
                if board.is_winner():
                    if len(self) - len(ignored_boards) > 1:
                        ignored_boards.append(i)
                        continue
                    return board.score() * draw

class AOCYear2021Day4(AdventOfCode):
    year = 2021
    day = 4

    def parse_data(self, data):
        rows = data.splitlines()
        draws = [int(i.strip()) for i in rows[0].split(',')]
        boards = []
        for row in rows[1:]:
            if not row.strip():
                boards.append([])
                continue
            boards[-1].append([int(s.strip()) for s in row.split() if s])

        boards = [Board(data) for data in boards]

        return Game(boards, draws)

    async def solution_1(self):
        return self.data.play_to_win()

    async def solution_2(self):
        return self.data.play_to_lose()
