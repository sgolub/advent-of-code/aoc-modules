from aoc import AdventOfCode


class AOCYear2021Day1(AdventOfCode):
    year = 2021
    day = 1

    async def solution_1(self):
        """Counting the number of times a depth increases from previous measurement"""
        data = self.data
        last = data[0]
        inc = 0
        for m in data[1:]:
            if m > last:
                inc += 1
            last = m
        return inc

    async def solution_2(self):
        data = self.data
        last = sum(data[0:3])
        inc = 0
        for i in range(1, len(data) - 2):
            v = sum(data[i:i+3])
            if v > last:
                inc += 1
            last = v
        return inc
