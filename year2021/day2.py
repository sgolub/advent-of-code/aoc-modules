from aoc import AdventOfCode


class AOCYear2021Day2(AdventOfCode):
    year = 2021
    day = 2

    def parse_data(self, data):
        return [{'dir': o.split()[0], 'qty': int(o.split()[1])} for o in data]

    async def solution_1(self):
        """Depth and horizontal postions read literally."""
        h_pos = 0
        depth = 0
        for d in self.data:
            if d['dir'] == "forward":
                h_pos += d['qty']
            elif d['dir'] == "down":
                depth += d['qty']
            elif d['dir'] == "up":
                depth -= d['qty']
        return h_pos * depth

    async def solution_2(self):
        """Incorporate aim into how horizontal positions are calculated."""
        h_pos = 0
        depth = 0
        aim = 0
        for d in self.data:
            if d['dir'] == "forward":
                h_pos += d['qty']
                depth += d['qty'] * aim
            elif d['dir'] == "down":
                aim += d['qty']
            elif d['dir'] == "up":
                aim -= d['qty']
        return h_pos * depth
