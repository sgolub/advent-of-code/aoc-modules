# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

import os

project = 'Advent of Code Solutions'
copyright = '2022, Stephen Golub'
author = 'Stephen Golub'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "autoapi.extension",
    "autodocsumm",
    "myst_parser",
    "sphinx_design",
    "sphinx.ext.autodoc",
    "sphinx.ext.coverage",
    "sphinx.ext.intersphinx",
    "sphinx.ext.napoleon",
    "sphinx.ext.viewcode",
    "sphinxcontrib.runcmd",
]

templates_path = ['_templates']
exclude_patterns = [
    "./**/__pypackages__/",
]

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'furo'
html_static_path = ['_static']

dir_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '../../'))
autoapi_dirs = []
for path in os.listdir(dir_path):
    if path.startswith("year"):
        for day_path in os.listdir(os.path.join(dir_path, path)):
            autoapi_dirs.append(os.path.join(dir_path, path))
def _sort_day(path):
    base, day = path.split('/')[-2:]
    try:
        day = int(day.replace("day", ""))
    except ValueError:
        pass
    else:
        day = f"{day:02d}"
    return f"{base}/{day}"
autoapi_dirs.sort()
autoapi_python_use_implicit_namespaces = True
autoapi_ignore = [
    "**/__pypackages__/**",
]
# autoapi_generate_api_docs = False
autoapi_template_dir = "_templates"
autoapi_python_class_content = "both"
autoapi_options = [
    'members',
    'undoc-members',
    'private-members',
    'show-inheritance',
    'special-members',
    'imported-members',
]
autodoc_typehints = "description"
autodoc_typehints_format = "short"

add_module_names = False

intersphinx_mapping = {
    'aoc': ("https://sgolub.gitlab.io/advent-of-code/advent-of-code-python-base/", None),
    'python': ('https://docs.python.org/3', None),
    'numpy': ('https://numpy.org/doc/stable/', None),
}
