# Advent of Code Solutions
```{toctree}
---
titlesonly:
maxdepth: 1
---

autoapi/year2015
autoapi/year2021
autoapi/year2022
```
