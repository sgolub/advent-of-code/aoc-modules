from collections import defaultdict
from itertools import permutations
import re

from aoc import AdventOfCode



class InvalidConnection(Exception):
    pass


class Person:
    def __init__(self, name, neighbors={}):
        self.name = name
        self.neighbors = defaultdict(int)
        self.neighbors.update(neighbors)

    def add_neighbor(self, name, happiness=0):
        self.neighbors[name] = happiness

    @property
    def happiest_neighbor_obj(self):
        return max(self.neighbors.items(), key=lambda x: x[1])

    @property
    def happiest_neighbor_name(self):
        return self.happiest_neighbor_obj[0]

    @property
    def happiest_neighbor_value(self):
        return self.happiest_neighbor_obj[1]

    @property
    def happiest_neighbor(self):
        return People[self.happiest_neighbor_name]

    def __str__(self):
        return self.name


class People(dict):
    def __getitem__(self, name):
        return self.get(name, Person(name))


class AOCYear2015Day13(AdventOfCode):
    year = 2015
    day = 13

    reg = re.compile(r"(\w+) would (\w+) (\d+) happiness units by sitting next to (\w+).")

    def parse_data(self, data):
        people = People()
        for d in data:
            m = self.reg.match(d)
            influenced = m.group(1)
            unit = 1 if m.group(2) == "gain" else -1
            val = int(m.group(3)) * unit
            neighbor = m.group(4)
            p = people[influenced]
            p.add_neighbor(neighbor, val)
            people[influenced] = p
        return people

    @staticmethod
    def _calc_happiness(data, a, b):
        print(data, a, b)
        if a in data and b in data[a]:
            return data[a][b]
        elif b in data and a in data[b]:
            return data[b][a]
        return None

    def _calc_haps(self):
        data = self.data
        people_counts = dict()
        for people in permutations(data.keys()):
            key = '-'.join(people)
            total = 0
            for person_name in people:
                for p_name, person in People.items():
                    if person_name == p_name:
                        continue
                    other = People[person_name]
                    if other.happiest_neighbor == person:
                        people_counts[person_name] = other.happiest_neighbor_value
            try:
                last_person = None
                for person in people:
                    if last_person is None:
                        last_person = person
                        continue
                    c = self._calc_happiness(data, person, last_person)
                    if c is None:
                        raise InvalidConnection(f"{person} -> {last_person}")
                    total += c
                    last_person = person
            except InvalidConnection:
                continue
            else:
                people_counts[key] = total
        return people_counts

    async def solution_1(self):
        for name, p in self.data.items():
            print(f"{name}: {p.happiest_neighbor}")

    async def solution_2(self):
        raise NotImplementedError
