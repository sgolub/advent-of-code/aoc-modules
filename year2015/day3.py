from collections import defaultdict

from aoc import AdventOfCode


x = 0
y = 1

dirMap = {
    '^': 1,
    'v': -1,
    '<': -1,
    '>': 1,
}

vMap = {
    '^': y,
    'v': y,
    '<': x,
    '>': x,
}

class AOCYear2015Day3(AdventOfCode):
    year = 2015
    day = 3

    def solution_1(self):
        pos = [0,0]
        result = defaultdict(int)
        result["0-0"] = 1
        for d in self.data:
            pos[vMap[d]] += dirMap[d]
            result["-".join(map(str, pos))] += 1
        return len(result)

    def solution_2(self):
        sPos = [0,0]
        rPos = [0,0]
        result = defaultdict(int)
        result["0-0"] = 1
        for i, d in enumerate(self.data):
            if i % 2 == 0:
                sPos[vMap[d]] += dirMap[d]
                result["-".join(map(str, sPos))] += 1
            else:
                rPos[vMap[d]] += dirMap[d]
                result["-".join(map(str, rPos))] += 1
        return len(result)
