from aoc import AdventOfCode


turn_on = True
turn_off = False
toggle = None

class AOCYear2015Day6(AdventOfCode):
    year = 2015
    day = 6

    def parse_data(self, data):
        result = []
        for v in data:
            d = {}
            if "turn on" in v:
                d['action'] = turn_on
                v = v.replace("turn on", "").strip()
            elif "turn off" in v:
                d['action'] = turn_off
                v = v.replace("turn off", "").strip()
            else:
                d['action'] = toggle
                v = v.replace("toggle", "").strip()

            c = v.split(" through ")
            d["start"] = list(map(int, c[0].split(",")))
            d["end"] = list(map(int, c[1].split(",")))
            result.append(d)
        return result

    @staticmethod
    def _get_result1(current, action):
        if action == turn_on or action == turn_off:
            return action
        else:
            return not current

    @staticmethod
    def _count(lights):
        return sum([sum(r) for r in lights])

    async def solution_1(self):
        lights = [[False for x in range(0, 1000)] for x in range(0, 1000)]
        for a in self.data:
            sx, sy = a['start']
            fx, fy = a['end']
            for x in range(sx, fx+1):
                for y in range(sy, fy+1):
                    lights[x][y] = self._get_result1(lights[x][y], a['action'])
            if self.use_sample:
                print(f"count: {self._count(lights)}")
        return self._count(lights)

    @staticmethod
    def _get_result2(action):
        if action == turn_on:
            return 1
        elif action == turn_off:
            return -1
        else:
            return 2

    async def solution_2(self):
        lights = [[0 for x in range(0, 1000)] for x in range(0, 1000)]
        for a in self.data:
            sx, sy = a['start']
            fx, fy = a['end']
            for x in range(sx, fx+1):
                for y in range(sy, fy+1):
                    lights[x][y] += self._get_result2(a['action'])
                    if lights[x][y] < 0:
                        lights[x][y] = 0

            if self.use_sample:
                print(f"count: {self._count(lights)}")
        return self._count(lights)
