import re

from aoc import AdventOfCode


class AOCYear2015Day11(AdventOfCode):
    year = 2015
    day = 11

    trip_straight = [f"{chr(i)!s}{chr(i+1)!s}{chr(i+2)}" for i in range(97, 97+23)]
    bad_letters = set("iol")
    reg = re.compile(r".*(\w)\1.*(\w)\2.*")

    @classmethod
    def _increment_char(cls, c):
        if c == "z":
            return "a"
        else:
            i = ord(c) + 1
            while chr(i) in cls.bad_letters:
                i += 1
            return chr(i)

    @classmethod
    def _contains_bad(cls, pw):
        return len(set(pw) - cls.bad_letters) != len(set(pw))

    @classmethod
    def _increment_pass(cls, pw):
        i = -1
        while True:
            c = cls._increment_char(pw[i])
            pw = pw[:i] + c + (pw[i+1:] if i != -1 else '')
            if c != 'a':
                break
            i -= 1
        if cls._contains_bad(pw):
            for i, c in enumerate(pw):
                if c in cls.bad_letters:
                    pw = pw[:i] + cls._increment_char(c) + "a" * (len(pw) - i - 1)
                    break
        return pw

    @classmethod
    def _valid_password(cls, password):
        numeric = list(map(ord, password))

        return (
            # Include an increasing subsequence
            any(
                numeric[i] + 2 == numeric[i + 1] + 1 == numeric[i + 2]
                for i in range(len(password) - 2)
            )
            # May not contain i, o, or l
            and not any(c in password for c in 'iol')
            # Must have at least two different pairs
            and len(set(re.findall(r'(.)\1', password))) >= 2
        )
        valid = False
        for s in cls.trip_straight:
            if s in pw:
                valid = True
                break

        if valid is False:
            return False

        if cls._contains_bad(pw):
            return False

        m = cls.reg.match(pw)
        if m is None:
            return False
        return m.group(1) != m.group(2)

    async def solution_1(self):
        pw = self._increment_pass(self.data)
        while not self._valid_password(pw):
            pw = self._increment_pass(pw)
        return pw

    async def solution_2(self):
        pw = self._increment_pass(await self.solution_1())
        while not self._valid_password(pw):
            pw = self._increment_pass(pw)
        return pw
