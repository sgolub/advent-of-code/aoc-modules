import json
import re

from aoc import AdventOfCode



class AOCYear2015Day12(AdventOfCode):
    year = 2015
    day = 12

    reg = re.compile(r"-?\d+")

    @classmethod
    def _contains_ignore(cls, data, ignore_key):
        return ignore_key in str(data)

    @classmethod
    def _trim_obj(cls, data):
        if isinstance(data, dict):
            if ("red" in data.keys() or "red" in data.values()):
                return {}
            return {key: cls._trim_obj(d) for key, d in data.items() if key != "red"}
        elif isinstance(data, list):
            return [cls._trim_obj(d) for d in data]
        return data

    async def solution_1(self):
        nums = self.reg.findall(self.data)
        if nums:
            return sum(map(int, nums))

    async def solution_2(self):
        data = json.loads(self.data)
        nums = self.reg.findall(json.dumps(self._trim_obj(data)))
        return sum(map(int, nums))
