from aoc import AdventOfCode


class AOCYear2015Day8(AdventOfCode):
    year = 2015
    day = 8

    def parse_data(self, data):
        return data.strip().splitlines()

    async def solution_1(self):
        chars = 0
        for line in self.data:
            chars += len(line)
            chars -= len(eval(line))
        return chars

    async def solution_2(self):
        chars = 0
        for line in self.data:
            chars += len(line[1:-1].replace("\\", "\\\\").replace('"', "\\\"")) + 6
            chars -= len(line)
        return chars
