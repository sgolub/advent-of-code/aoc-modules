from collections import Counter
import re

from aoc import AdventOfCode


class AOCYear2015Day5(AdventOfCode):
    year = 2015
    day = 5

    naughty_combos = ('ab', 'cd', 'pq', 'xy', )
    r1 = re.compile(r'.*(\w\w).*\1.*')
    r2 = re.compile(r'.*(\w)\w\1.*')

    @classmethod
    def _naughty1(cls, val):
        for n in cls.naughty_combos:
            if n in val:
                return True
        return False

    @classmethod
    def _nice1(cls, val) -> bool:
        val = val.lower()
        if cls._naughty1(val):
            return False
        nice = False
        for i in range(97, 97+26):
            if chr(i) * 2 in val:
                nice = True
                break
        if not nice:
            return False
        c = Counter(val)
        if c['a'] + c['e'] + c['i'] + c['o'] + c['u'] >= 3:
            return True
        return False

    async def solution_1(self):
        result = 0
        for v in self.data:
            n = self._nice1(v)
            if self.use_sample:
                print(f"{v}: {n}")
            result += int(n)
        return result

    @classmethod
    def _nice2(cls, val) -> bool:
        return cls.r1.match(val) is not None and cls.r2.match(val) is not None

    async def solution_2(self):
        result = 0
        for v in self.data:
            n = self._nice2(v)
            if self.use_sample:
                print(f"{v}: {n}")
            result += int(n)
        return result
