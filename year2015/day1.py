from aoc import AdventOfCode


class AOCYear2015Day1(AdventOfCode):
    year = 2015
    day = 1

    def parse_data(self, data):
        return list(data.strip())

    @staticmethod
    def _get_inc(c):
        return -1 if c == ")" else 1

    async def solution_1(self):
        floor = 0
        for d in self.data:
            floor += self._get_inc(d)
        return floor

    async def solution_2(self):
        floor = 0
        for p, d in enumerate(self.data):
            floor += self._get_inc(d)
            if floor < 0:
                return p + 1
