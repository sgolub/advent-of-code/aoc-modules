from collections import defaultdict
from itertools import permutations
import re

from aoc import AdventOfCode



class InvalidRoute(Exception):
    pass

class AOCYear2015Day9(AdventOfCode):
    year = 2015
    day = 9

    reg = re.compile(r"(\w+) to (\w+) = (\d+)")
    def parse_data(self, data):
        routes = defaultdict(dict)
        locations = set()
        for d in data:
            m = self.reg.match(d)
            routes[m.group(1)][m.group(2)] = int(m.group(3))
            locations.add(m.group(1))
            locations.add(m.group(2))
        return {
            'routes': dict(routes),
            'locations': locations,
        }

    @staticmethod
    def _connect(routes, a, b):
        if a in routes and b in routes[a]:
            return routes[a][b]
        elif b in routes and a in routes[b]:
            return routes[b][a]
        return None

    def _calc_routes(self):
        routes = self.data['routes']
        locs = self.data['locations']
        route_combos = permutations(locs)
        route_lengths = dict()
        for route in route_combos:
            length = 0
            key = '-'.join(route)
            try:
                last_loc = None
                for loc in route:
                    if last_loc is None:
                        last_loc = loc
                        continue
                    distance = self._connect(routes, loc, last_loc)
                    if distance is None:
                        raise InvalidRoute(f"{loc} -> {last_loc}")
                    length += distance
                    last_loc = loc
            except InvalidRoute as e:
                continue
            else:
                route_lengths[key] = length
        return route_lengths

    async def solution_1(self):
        min_item = min(self._calc_routes().items(), key=lambda x: x[1])
        print(min_item[0])
        return min_item[1]

    async def solution_2(self):
        max_item = max(self._calc_routes().items(), key=lambda x: x[1])
        print(max_item[0])
        return max_item[1]
