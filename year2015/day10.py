from aoc import AdventOfCode


class AOCYear2015Day10(AdventOfCode):
    year = 2015
    day = 10

    def _look_and_say(self, val, n=1):
        if n == 1:
            return val
        for i in range(1, n+1):
            j = 0
            temp = ""
            curr = ""
            count = 0
            while j < len(val):
                if curr == "":
                    curr = val[j]
                    count = 1
                    j += 1
                elif curr == val[j]:
                    count += 1
                    j += 1
                else:
                    temp += str(count) + curr
                    curr = ""
                    count = 0
            temp += str(count) + curr
            val = temp
        return val

    async def solution_1(self):
        return len(self._look_and_say(self.data, n=5 if self.use_sample else 40))

    async def solution_2(self):
        return len(self._look_and_say(self.data, n=50))
