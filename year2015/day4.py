from hashlib import md5

from aoc import AdventOfCode


class AOCYear2015Day4(AdventOfCode):
    year = 2015
    day = 4

    def _find_with_zeros(self, zero_count):
        start_str = "0" * zero_count
        data = self.data
        i = 0
        while True:
            h = md5(f"{data}{i}".encode())
            if h.hexdigest().startswith(start_str):
                return i
            i += 1

    async def solution_1(self):
        return self._find_with_zeros(5)

    async def solution_2(self):
        return self._find_with_zeros(6)
