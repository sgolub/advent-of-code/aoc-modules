from collections import defaultdict
import json
import operator
import re

from aoc import AdventOfCode


class AOCYear2015Day7(AdventOfCode):
    year = 2015
    day = 7

    actionMap = {
        "AND": "&",
        "OR": "|",
        "LSHIFT": "<<",
        "RSHIFT": ">>",
    }

    monops = {
        'NOT': lambda x : ~x & 0xFFFF,
    }

    binops = {
        'AND': operator.and_,
        'OR': operator.or_,
        'LSHIFT': operator.lshift,
        'RSHIFT': operator.rshift,
    }

    def __init__(self, *args, alter=False, **kwargs):
        super().__init__(*args, **kwargs)
        self.alter = alter

    def parse_data(self, data):
        machine = {}

        for line in data:
            line = line.strip()

            m = (
                re.match(r'(\w+) -> (\w+)', line)
                or re.match(r'(\w+) (\w+) (\w+) -> (\w+)', line)
                or re.match(r'(\w+) (\w+) -> (\w+)', line)
            ).groups()

            machine[m[-1]] = m[:-1]
        return machine

    def evaluate(self, register_or_value):
        try:
            return int(register_or_value)
        except:
            return self.run(register_or_value)

    def run(self, register, state = {}, alter=None):
        data = self.data
        if alter:
            data['b'] = alter
        if not register in state:
            command = data[register]

            if len(command) == 1:
                value, = command
                state[register] = self.evaluate(value)

            elif len(command) == 2:
                monop, input = command
                state[register] = self.monops[monop](self.evaluate(input))

            elif len(command) == 3:
                input_1, binop, input_2 = command
                state[register] = self.binops[binop](self.evaluate(input_1), self.evaluate(input_2))

        return state[register]

    async def solution_1(self):
        return self.run('a')

    async def solution_2(self):
        a = self.run('a')
        return self.run('a', alter=a)
