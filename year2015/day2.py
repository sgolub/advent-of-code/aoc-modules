from aoc import AdventOfCode


class AOCYear2015Day2(AdventOfCode):
    year = 2015
    day = 2

    def parse_data(self, data):
        return [list(map(int, d.strip().split("x"))) for d in data]

    @classmethod
    def _calc_area(cls, d):
        return sum([2*s for s in cls._sides(d)])

    @staticmethod
    def _sides(d):
        return (d[0]*d[1], d[1]*d[2], d[0]*d[2])

    @classmethod
    def _smallest_side(cls, d):
        return min(cls._sides(d))

    @classmethod
    def _small_perimieter(cls, d):
        sides = sorted([2*s for s in d])[:2]
        r = sum(sides)
        return r

    @classmethod
    def _volume(cls, d):
        result = 1
        for s in d:
            result *= s
        return result

    async def solution_1(self):
        return sum([self._calc_area(d) + self._smallest_side(d) for d in self.data])

    async def solution_2(self):
        return sum([self._volume(d) + self._small_perimieter(d) for d in self.data])
