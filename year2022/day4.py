from aoc import AdventOfCode


class AOCYear2022Day4(AdventOfCode):
    year = 2022
    day = 4

    def parse_data(self, data):
        return [[[int(i) for i in range.split("-")] for range in line.strip().split(",")]for line in data.strip().splitlines()]

    @classmethod
    def _contains(cls, elfa, elfb, with_flip=True):
        if elfa[0] >= elfb[0] and elfa[1] <= elfb[1]:
            return True
        if with_flip:
            return cls._contains(elfb, elfa, with_flip=False)

    async def solution_1(self):
        count = 0
        for elfa, elfb in self.data:
            if self._contains(elfa, elfb):
                count += 1
        return count

    async def solution_2(self):
        count = 0
        for elfa, elfb in self.data:
            elfa_set = set(range(elfa[0], elfa[1] + 1))
            elfb_set = set(range(elfb[0], elfb[1] + 1))
            if len(elfa_set.intersection(elfb_set)) > 0:
                count += 1
        return count
