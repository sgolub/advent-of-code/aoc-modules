from collections import defaultdict
import re

from aoc import AdventOfCode


class AOCYear2022Day5(AdventOfCode):
    year = 2022
    day = 5
    move_re = re.compile(r"move (?P<count>\d+) from (?P<src>\d+) to (?P<dest>\d+)")

    def parse_data(self, data):
        raw_stacks = data['stacks'].splitlines()
        raw_moves = data['moves'].splitlines()
        stacks = defaultdict(list)
        for stack_row in raw_stacks:
            stack = 1
            for i in range(0, len(stack_row), 4):
                crate = stack_row[i:i+4]
                if '[' in crate:
                    stacks[stack].insert(0, crate[1])
                stack += 1
        return {
            'stacks': dict(sorted(stacks.items(), key=lambda x: x[0])),
            'moves': [{k: int(v) for k, v in self.move_re.match(move).groupdict().items()} for move in raw_moves],
        }

    async def solution_1(self):
        stacks = self.data['stacks']
        for move in self.data['moves']:
            for i in range(move['count']):
                stacks[move['dest']].append(stacks[move['src']].pop())
        return ''.join(v[-1] for v in stacks.values())


    async def solution_2(self):
        stacks = self.data['stacks']
        for move in self.data['moves']:
            moved = stacks[move['src']][0-move['count']:]
            stacks[move['dest']].extend(moved)
            stacks[move['src']] = stacks[move['src']][0:0-move['count']]

        return ''.join(v[-1] for v in stacks.values())
