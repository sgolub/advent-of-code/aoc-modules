from functools import cached_property

from aoc import AdventOfCode


equals = {
    "X": "A",
    "Y": "B",
    "Z": "C",
}
desired_result = {
    "X": False,
    "Y": None,
    "Z": True,
}
score_for_result = {
    True: 6,
    False: 0,
    None: 3,
}

class AOCYear2022Day2(AdventOfCode):
    year = 2022
    day = 2

    def parse_data(self, data):
        results = []
        for round in data.strip().splitlines():
            opp, me = round.strip().split(" ", 1)
            results.append(dict(opp=opp, me=me))
        return results

    async def solution_1(self):
        defeats = {
            "X": "C",
            "Y": "A",
            "Z": "B",
        }
        score_for_move = {
            "X": 1,
            "Y": 2,
            "Z": 3,
        }
        score = 0
        for round in self.data:
            # Score for my play
            score += score_for_move[round['me']]
            # Score if we played the same
            if equals[round['me']] == round['opp']:
                score += score_for_result[None]
            else:
                score += score_for_result[defeats[round['me']] == round['opp']]
        return score

    async def solution_2(self):
        defeats = {
            "A": "C",
            "B": "A",
            "C": "B",
        }
        score_for_move = {
            "A": 1,
            "B": 2,
            "C": 3,
        }
        score = 0
        defeated_by = {v: k for k, v in defeats.items()}
        for round in self.data:
            dr = desired_result[round["me"]]
            score += score_for_result[dr]
            if dr is None:
                score += score_for_move[round["opp"]]
            elif dr:
                score += score_for_move[defeated_by[round["opp"]]]
            else:
                score += score_for_move[defeats[round["opp"]]]
        return score
