import enum
from functools import cached_property
import typing as t

from aoc import AdventOfCode



class Cell(enum.Enum):
    START = 0
    EMPTY = 1
    ROCK = 2
    SAND = 3

    def __str__(self):
        match self:
            case Cell.START:
                return "+"
            case Cell.EMPTY:
                return "."
            case Cell.ROCK:
                return "#"
            case Cell.SAND:
                return "o"


class Floor(enum.IntEnum):
    INFINITE = 0
    FINITE = 1


class Cave:
    def __init__(self, cave_scan: t.Dict[t.Tuple[int, int], Cell], start: t.Tuple[int, int],
                 extremes: t.Tuple[t.Tuple[int, int], t.Tuple[int, int]], floor_type: Floor = Floor.INFINITE):
        self.cave_scan = cave_scan
        self.start = start
        self.cave_scan[start] = Cell.START
        self.floor_type = floor_type
        self._extremes = ((extremes[0][0], 0), extremes[1])

    @cached_property
    def extremes(self):
        if self.floor_type == Floor.INFINITE:
            return self._extremes
        max_y = self._extremes[1][1] + 1
        self._extremes = ((self.start[0] - max_y, 0), (self.start[0] + max_y, max_y))
        return self._extremes

    @cached_property
    def bottom_depth(self) -> int:
        return self.extremes[1][1] + int(self.floor_type) - 1

    def cave(self, x: int, y: int) -> Cell:
        return self.cave_scan.get((x, y), Cell.EMPTY)

    def _pour(self, x: int, y: int) -> int:
        if self.cave(x, y + 1) == Cell.EMPTY:
            return (x, y + 1)
        elif self.cave(x - 1, y + 1) == Cell.EMPTY:
            return (x - 1, y + 1)
        elif self.cave(x + 1, y + 1) == Cell.EMPTY:
            return (x + 1, y + 1)
        else:
            return (x, y)

    def pour_to_infinite(self) -> int:
        sand_count = 0
        while True:
            sand_count += 1
            x, y = self.start
            while True:
                if y > self.bottom_depth:
                    return sand_count - 1
                new_x, new_y = self._pour(x, y)
                if new_x == x and new_y == y:
                    self.cave_scan[(x, y)] = Cell.SAND
                    break
                else:
                    x, y = new_x, new_y
        return sand_count

    def pour_to_bottom(self) -> int:
        sand_count = 0
        while True:
            sand_count += 1
            x, y = self.start
            while True:
                if y + 1 > self.bottom_depth:
                    self.cave_scan[(x, y)] = Cell.SAND
                    break
                if self.cave(499, 1) == self.cave(500, 1) == self.cave(501, 1) == Cell.SAND:
                    return sand_count
                new_x, new_y = self._pour(x, y)
                if new_x == x and new_y == y and (new_x, new_y) != self.start:
                    self.cave_scan[(x, y)] = Cell.SAND
                    break
                else:
                    x, y = new_x, new_y

    def pour(self) -> int:
        if self.floor_type == Floor.INFINITE:
            return self.pour_to_infinite()
        else:
            return self.pour_to_bottom()

    def __str__(self):
        lines = []
        for y in range(self.extremes[0][1], self.extremes[1][1] + 1):
            lines.append([])
            for x in range(self.extremes[0][0], self.extremes[1][0] + 1):
                lines[-1].append(self.cave_scan.get((x, y), Cell.EMPTY))
            lines[-1] = "".join(map(str, lines[-1]))
        return "\n".join(lines)


class AOCYear2022Day14(AdventOfCode):
    year = 2022
    day = 14

    def parse_data(self, data):
        cave_scan = {}
        rock_paths = []
        max_x = -1
        max_y = -1
        min_x = -1
        min_y = -1
        for line in data.splitlines():
            rock_path = [[int(p) for p in path.split(",")] for path in line.split(" -> ")]
            rock_paths.append(rock_path)
            for i in range(len(rock_path) - 1):
                start_x, start_y = rock_path[i]
                end_x, end_y = rock_path[i + 1]
                if start_x > max_x or max_x == -1:
                    max_x = start_x
                elif start_x < min_x or min_x == -1:
                    min_x = start_x
                if start_y > max_y or max_y == -1:
                    max_y = start_y
                elif start_y < min_y or min_y == -1:
                    min_y = start_y
                if end_x > max_x or max_x == -1:
                    max_x = end_x
                elif end_x < min_x or min_x == -1:
                    min_x = end_x
                if end_y > max_y or max_y == -1:
                    max_y = end_y
                elif end_y < min_y or min_y == -1:
                    min_y = end_y
                if start_x == end_x:
                    if start_y > end_y:
                        start_y, end_y = end_y, start_y
                    for y in range(start_y, end_y + 1):
                        cave_scan[(start_x, y)] = Cell.ROCK
                else:
                    if start_x > end_x:
                        start_x, end_x = end_x, start_x
                    for x in range(start_x, end_x + 1):
                        cave_scan[(x, start_y)] = Cell.ROCK
        return Cave(cave_scan, (500, 0), extremes=((min_x, min_y), (max_x, max_y)))

    async def solution_1(self):
        """The distress signal leads you to a giant waterfall! Actually, hang on - the signal seems like it's coming from the waterfall itself, and that doesn't make any sense. However, you do notice a little path that leads behind the waterfall.

        Correction: the distress signal leads you behind a giant waterfall! There seems to be a large cave system here, and the signal definitely leads further inside.

        As you begin to make your way deeper underground, you feel the ground rumble for a moment. Sand begins pouring into the cave! If you don't quickly figure out where the sand is going, you could quickly become trapped!

        Fortunately, your familiarity with analyzing the path of falling material will come in handy here. You scan a two-dimensional vertical slice of the cave above you (your puzzle input) and discover that it is mostly air with structures made of rock.

        Your scan traces the path of each solid rock structure and reports the x,y coordinates that form the shape of the path, where x represents distance to the right and y represents distance down. Each path appears as a single line of text in your scan. After the first point of each path, each point indicates the end of a straight horizontal or vertical line to be drawn from the previous point. For example:

        498,4 -> 498,6 -> 496,6
        503,4 -> 502,4 -> 502,9 -> 494,9
        This scan means that there are two paths of rock; the first path consists of two straight lines, and the second path consists of three straight lines. (Specifically, the first path consists of a line of rock from 498,4 through 498,6 and another line of rock from 498,6 through 496,6.)

        The sand is pouring into the cave from point 500,0.

        Drawing rock as #, air as ., and the source of the sand as +, this becomes:


          4     5  5
          9     0  0
          4     0  3
        0 ......+...
        1 ..........
        2 ..........
        3 ..........
        4 ....#...##
        5 ....#...#.
        6 ..###...#.
        7 ........#.
        8 ........#.
        9 #########.
        Sand is produced one unit at a time, and the next unit of sand is not produced until the previous unit of sand comes to rest. A unit of sand is large enough to fill one tile of air in your scan.

        A unit of sand always falls down one step if possible. If the tile immediately below is blocked (by rock or sand), the unit of sand attempts to instead move diagonally one step down and to the left. If that tile is blocked, the unit of sand attempts to instead move diagonally one step down and to the right. Sand keeps moving as long as it is able to do so, at each step trying to move down, then down-left, then down-right. If all three possible destinations are blocked, the unit of sand comes to rest and no longer moves, at which point the next unit of sand is created back at the source.

        So, drawing sand that has come to rest as o, the first unit of sand simply falls straight down and then stops:

        ......+...
        ..........
        ..........
        ..........
        ....#...##
        ....#...#.
        ..###...#.
        ........#.
        ......o.#.
        #########.
        The second unit of sand then falls straight down, lands on the first one, and then comes to rest to its left:

        ......+...
        ..........
        ..........
        ..........
        ....#...##
        ....#...#.
        ..###...#.
        ........#.
        .....oo.#.
        #########.
        After a total of five units of sand have come to rest, they form this pattern:

        ......+...
        ..........
        ..........
        ..........
        ....#...##
        ....#...#.
        ..###...#.
        ......o.#.
        ....oooo#.
        #########.
        After a total of 22 units of sand:

        ......+...
        ..........
        ......o...
        .....ooo..
        ....#ooo##
        ....#ooo#.
        ..###ooo#.
        ....oooo#.
        ...ooooo#.
        #########.
        Finally, only two more units of sand can possibly come to rest:

        ......+...
        ..........
        ......o...
        .....ooo..
        ....#ooo##
        ...o#ooo#.
        ..###ooo#.
        ....oooo#.
        .o.ooooo#.
        #########.
        Once all 24 units of sand shown above have come to rest, all further sand flows out the bottom, falling into the endless void. Just for fun, the path any new sand takes before falling forever is shown here with ~:

        .......+...
        .......~...
        ......~o...
        .....~ooo..
        ....~#ooo##
        ...~o#ooo#.
        ..~###ooo#.
        ..~..oooo#.
        .~o.ooooo#.
        ~#########.
        ~..........
        ~..........
        ~..........
        Using your scan, simulate the falling sand. How many units of sand come to rest before sand starts flowing into the abyss below?
        """
        return self.data.pour()

    async def solution_2(self):
        """You realize you misread the scan. There isn't an endless void at the bottom of the scan - there's floor, and you're standing on it!

        You don't have time to scan the floor, so assume the floor is an infinite horizontal line with a y coordinate equal to two plus the highest y coordinate of any point in your scan.

        In the example above, the highest y coordinate of any point is 9, and so the floor is at y=11. (This is as if your scan contained one extra rock path like -infinity,11 -> infinity,11.) With the added floor, the example above now looks like this:

                ...........+........
                ....................
                ....................
                ....................
                .........#...##.....
                .........#...#......
                .......###...#......
                .............#......
                .............#......
                .....#########......
                ....................
        <-- etc #################### etc -->
        To find somewhere safe to stand, you'll need to simulate falling sand until a unit of sand comes to rest at 500,0, blocking the source entirely and stopping the flow of sand into the cave. In the example above, the situation finally looks like this after 93 units of sand come to rest:

        ............o............
        ...........ooo...........
        ..........ooooo..........
        .........ooooooo.........
        ........oo#ooo##o........
        .......ooo#ooo#ooo.......
        ......oo###ooo#oooo......
        .....oooo.oooo#ooooo.....
        ....oooooooooo#oooooo....
        ...ooo#########ooooooo...
        ..ooooo.......ooooooooo..
        #########################
        Using your scan, simulate the falling sand until the source of the sand becomes blocked. How many units of sand come to rest?
        """
        cave = self.data
        cave.floor_type = Floor.FINITE
        return cave.pour()
