from __future__ import annotations

from functools import cmp_to_key
from itertools import zip_longest

from aoc import AdventOfCode



def compare(left, right):
    if left is None:
        return -1
    if right is None:
        return 1

    if isinstance(left, int) and isinstance(right, int):
        if left < right:
            return -1
        elif left > right:
            return 1
        else:
            return 0
    elif isinstance(left, list) and isinstance(right, list):
        for l2, r2 in zip_longest(left, right):
            if (result := compare(l2, r2)) != 0:
                return result
        return 0
    else:
        l2 = [left] if isinstance(left, int) else left
        r2 = [right] if isinstance(right, int) else right
        return compare(l2, r2)

class Pairs:
    def __init__(self, item_1, item_2):
        self.item_1 = item_1
        self.item_2 = item_2

    def compare(self):
        return compare(self.item_1, self.item_2)

    @property
    def valid(self):
        return self.compare() == -1


class AOCYear2022Day13(AdventOfCode):
    """Distress Signal"""
    year = 2022
    day = 13

    def parse_data(self, data):
        return [eval(line.strip()) for line in data.splitlines() if line.strip()]

    async def solution_1(self):
        """You climb the hill and again try contacting the Elves. However, you instead receive a signal you weren't expecting: a distress signal.

        Your handheld device must still not be working properly; the packets from the distress signal got decoded out of order. You'll need to re-order the list of received packets (your puzzle input) to decode the message.

        Your list consists of pairs of packets; pairs are separated by a blank line. You need to identify how many pairs of packets are in the right order.

        For example:

        [1,1,3,1,1]
        [1,1,5,1,1]

        [[1],[2,3,4]]
        [[1],4]

        [9]
        [[8,7,6]]

        [[4,4],4,4]
        [[4,4],4,4,4]

        [7,7,7,7]
        [7,7,7]

        []
        [3]

        [[[]]]
        [[]]

        [1,[2,[3,[4,[5,6,7]]]],8,9]
        [1,[2,[3,[4,[5,6,0]]]],8,9]
        Packet data consists of lists and integers. Each list starts with [, ends with ], and contains zero or more comma-separated values (either integers or other lists). Each packet is always a list and appears on its own line.

        When comparing two values, the first value is called left and the second value is called right. Then:

        If both values are integers, the lower integer should come first. If the left integer is lower than the right integer, the inputs are in the right order. If the left integer is higher than the right integer, the inputs are not in the right order. Otherwise, the inputs are the same integer; continue checking the next part of the input.
        If both values are lists, compare the first value of each list, then the second value, and so on. If the left list runs out of items first, the inputs are in the right order. If the right list runs out of items first, the inputs are not in the right order. If the lists are the same length and no comparison makes a decision about the order, continue checking the next part of the input.
        If exactly one value is an integer, convert the integer to a list which contains that integer as its only value, then retry the comparison. For example, if comparing [0,0,0] and 2, convert the right value to [2] (a list containing 2); the result is then found by instead comparing [0,0,0] and [2].
        Using these rules, you can determine which of the pairs in the example are in the right order:

        == Pair 1 ==
        - Compare [1,1,3,1,1] vs [1,1,5,1,1]
          - Compare 1 vs 1
          - Compare 1 vs 1
          - Compare 3 vs 5
            - Left side is smaller, so inputs are in the right order

        == Pair 2 ==
        - Compare [[1],[2,3,4]] vs [[1],4]
          - Compare [1] vs [1]
            - Compare 1 vs 1
          - Compare [2,3,4] vs 4
            - Mixed types; convert right to [4] and retry comparison
            - Compare [2,3,4] vs [4]
              - Compare 2 vs 4
                - Left side is smaller, so inputs are in the right order

        == Pair 3 ==
        - Compare [9] vs [[8,7,6]]
          - Compare 9 vs [8,7,6]
            - Mixed types; convert left to [9] and retry comparison
            - Compare [9] vs [8,7,6]
              - Compare 9 vs 8
                - Right side is smaller, so inputs are not in the right order

        == Pair 4 ==
        - Compare [[4,4],4,4] vs [[4,4],4,4,4]
          - Compare [4,4] vs [4,4]
            - Compare 4 vs 4
            - Compare 4 vs 4
          - Compare 4 vs 4
          - Compare 4 vs 4
          - Left side ran out of items, so inputs are in the right order

        == Pair 5 ==
        - Compare [7,7,7,7] vs [7,7,7]
          - Compare 7 vs 7
          - Compare 7 vs 7
          - Compare 7 vs 7
          - Right side ran out of items, so inputs are not in the right order

        == Pair 6 ==
        - Compare [] vs [3]
          - Left side ran out of items, so inputs are in the right order

        == Pair 7 ==
        - Compare [[[]]] vs [[]]
          - Compare [[]] vs []
            - Right side ran out of items, so inputs are not in the right order

        == Pair 8 ==
        - Compare [1,[2,[3,[4,[5,6,7]]]],8,9] vs [1,[2,[3,[4,[5,6,0]]]],8,9]
          - Compare 1 vs 1
          - Compare [2,[3,[4,[5,6,7]]]] vs [2,[3,[4,[5,6,0]]]]
            - Compare 2 vs 2
            - Compare [3,[4,[5,6,7]]] vs [3,[4,[5,6,0]]]
              - Compare 3 vs 3
              - Compare [4,[5,6,7]] vs [4,[5,6,0]]
                - Compare 4 vs 4
                - Compare [5,6,7] vs [5,6,0]
                  - Compare 5 vs 5
                  - Compare 6 vs 6
                  - Compare 7 vs 0
                    - Right side is smaller, so inputs are not in the right order
        What are the indices of the pairs that are already in the right order? (The first pair has index 1, the second pair has index 2, and so on.) In the above example, the pairs in the right order are 1, 2, 4, and 6; the sum of these indices is 13.

        Determine which pairs of packets are already in the right order. What is the sum of the indices of those pairs?
        """
        data = self.data
        pairs = []
        for i in range(0, len(data), 2):
            pairs.append(Pairs(data[i], data[i + 1]))

        return sum([i+1 for i, p in enumerate(pairs) if p.valid])

    async def solution_2(self):
        """Now, you just need to put all of the packets in the right order. Disregard the blank lines in your list of received packets.

        The distress signal protocol also requires that you include two additional divider packets:

        [[2]]
        [[6]]
        Using the same rules as before, organize all packets - the ones in your list of received packets as well as the two divider packets - into the correct order.

        For the example above, the result of putting the packets in the correct order is:

        []
        [[]]
        [[[]]]
        [1,1,3,1,1]
        [1,1,5,1,1]
        [[1],[2,3,4]]
        [1,[2,[3,[4,[5,6,0]]]],8,9]
        [1,[2,[3,[4,[5,6,7]]]],8,9]
        [[1],4]
        [[2]]
        [3]
        [[4,4],4,4]
        [[4,4],4,4,4]
        [[6]]
        [7,7,7]
        [7,7,7,7]
        [[8,7,6]]
        [9]
        Afterward, locate the divider packets. To find the decoder key for this distress signal, you need to determine the indices of the two divider packets and multiply them together. (The first packet is at index 1, the second packet is at index 2, and so on.) In this example, the divider packets are 10th and 14th, and so the decoder key is 140.

        Organize all of the packets into the correct order. What is the decoder key for the distress signal?
        """
        div1, div2 = [[2]], [[6]]
        sorted_packets = sorted([*self.data, div1, div2], key=cmp_to_key(compare))
        return (sorted_packets.index(div1) + 1) * (sorted_packets.index(div2) + 1)
