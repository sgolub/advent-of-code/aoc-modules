import typing as t

from aoc import AdventOfCode



class Map:
    def __init__(
            self,
            grid: t.Dict[t.Tuple[int, int], int],
            start: t.Tuple[int, int] = None,
            end: t.Tuple[int, int] = None,
            min_heights: t.List[t.Tuple[int, int]] = None,
        ):
        self.heightmap = grid
        self.grid = grid
        if start is None:
            start = self.find("S")
        self.start = start
        if end is None:
            end = self.find("E")
        self.end = end
        if min_heights is None:
            min_heights = list(self.find_all(['a', 'S']))
        self.min_heights = min_heights

    @staticmethod
    def adjecents(xy):
        (x, y) = xy
        return [(x+1, y), (x-1, y), (x, y+1), (x, y-1)]
        return [(x - 1, y), (x + 1, y), (x, y - 1), (x, y + 1)]

    def is_possible_move(self, xy, nxy):
        return ord(self.grid[nxy].replace("E", 'z')) - ord(self.grid[xy].replace("S", "a")) <= 1 if nxy in self.grid else False

    def possible_adjecents(self, xy):
        return (nxy for nxy in self.adjecents(xy) if self.is_possible_move(xy, nxy))

    def flood(self, dist, layers):
        edge = set(nxy for xy in layers[-1]
                       for nxy in self.possible_adjecents(xy) if nxy not in dist)

        dist.update({ xy: len(layers) for xy in edge })

        if edge:
            self.flood(dist, layers + [edge])

    def distance(self, start: t.Optional[int] = None, end: t.Optional[int] = None):
        if start is None:
            start = self.start
        if end is None:
            end = self.end
        dist = {
            start: 0,
        }
        self.flood(dist, [{start}])
        return dist[end] if end in dist else 9999

    def find_all(self, values):
        return (xy for (xy, v) in self.grid.items() if v in values)

    def find(self, value):
        return next(self.find_all(value))


class AOCYear2022Day12(AdventOfCode):
    """Hill Climbing Algorithm"""
    year = 2022
    day = 12

    def parse_data(self, data):
        return Map(
            {
                (x,y): c for y, line in enumerate(data.splitlines())
                for x, c in enumerate(line.strip())
            }
        )

    async def solution_1(self):
        """You try contacting the Elves using your handheld device, but the river you're following must be too low to get a decent signal.

        You ask the device for a heightmap of the surrounding area (your puzzle input). The heightmap shows the local area from above broken into a grid; the elevation of each square of the grid is given by a single lowercase letter, where a is the lowest elevation, b is the next-lowest, and so on up to the highest elevation, z.

        Also included on the heightmap are marks for your current position (S) and the location that should get the best signal (E). Your current position (S) has elevation a, and the location that should get the best signal (E) has elevation z.

        You'd like to reach E, but to save energy, you should do it in as few steps as possible. During each step, you can move exactly one square up, down, left, or right. To avoid needing to get out your climbing gear, the elevation of the destination square can be at most one higher than the elevation of your current square; that is, if your current elevation is m, you could step to elevation n, but not to elevation o. (This also means that the elevation of the destination square can be much lower than the elevation of your current square.)

        For example:

        Sabqponm
        abcryxxl
        accszExk
        acctuvwj
        abdefghi
        Here, you start in the top-left corner; your goal is near the middle. You could start by moving down or right, but eventually you'll need to head toward the e at the bottom. From there, you can spiral around to the goal:

        v..v<<<<
        >v.vv<<^
        .>vv>E^^
        ..v>>>^^
        ..>>>>>^
        In the above diagram, the symbols indicate whether the path exits each square moving up (^), down (v), left (<), or right (>). The location that should get the best signal is still E, and . marks unvisited squares.

        This path reaches the goal in 31 steps, the fewest possible.

        What is the fewest steps required to move from your current position to the location that should get the best signal?
        """
        m = self.data
        return m.distance(m.start, m.end)

    async def solution_2(self):
        """As you walk up the hill, you suspect that the Elves will want to turn this into a hiking trail. The beginning isn't very scenic, though; perhaps you can find a better starting point.

        To maximize exercise while hiking, the trail should start as low as possible: elevation a. The goal is still the square marked E. However, the trail should still be direct, taking the fewest steps to reach its goal. So, you'll need to find the shortest path from any square at elevation a to the square marked E.

        Again consider the example from above:

        Sabqponm
        abcryxxl
        accszExk
        acctuvwj
        abdefghi
        Now, there are six choices for starting position (five marked a, plus the square marked S that counts as being at elevation a). If you start at the bottom-left square, you can reach the goal most quickly:

        ...v<<<<
        ...vv<<^
        ...v>E^^
        .>v>>>^^
        >^>>>>>^
        This path reaches the goal in only 29 steps, the fewest possible.

        What is the fewest steps required to move starting from any square with elevation a to the location that should get the best signal?
        """
        m = self.data
        results = []
        for p in m.min_heights:
            m.start = p
            results.append(m.distance(m.start, m.end))
        return min(results)
