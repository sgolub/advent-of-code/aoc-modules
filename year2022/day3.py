import string

from aoc import AdventOfCode



priorization = {c: i + 1 for i, c in enumerate(string.ascii_lowercase + string.ascii_uppercase)}


class AOCYear2022Day3(AdventOfCode):
    year = 2022
    day = 3

    def parse_data(self, data):
        result = []
        for line in data.strip().splitlines():
            half = int(len(line) / 2)
            result.append([line[0:half], line[half:None]])
        return result

    async def solution_1(self):
        priorities = []
        for comp_1, comp_2 in self.data:
            priorities.append(priorization[list(set(comp_1).intersection(set(comp_2)))[0]])
        return sum(priorities)

    async def solution_2(self):
        priorities = []
        for i in range(0, len(self.data), 3):
            shared = set()
            for a, b in self.data[i:i+3]:
                if not shared:
                    shared = set(a + b)
                else:
                    shared = shared.intersection(set(a + b))
            assert len(shared) == 1, f"Expected only one common item, found {len(shared)}"
            priorities.append(priorization[list(shared)[0]])
        return sum(priorities)
