from aoc import AdventOfCode



class AOCYear2022Day15(AdventOfCode):
    year = 2022
    day = 15

    def parse_data(self, data):
        return data

    async def solution_1(self):
        raise NotImplementedError

    async def solution_2(self):
        raise NotImplementedError
