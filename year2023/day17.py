from heapq import heapify, heappop, heappush

from aoc import AdventOfCode
import numpy as np



class AOCYear2023Day17(AdventOfCode):
    """Day 17: Clumsy Crucible

    The lava starts flowing rapidly once the Lava Production Facility is operational. As you leave,
    the reindeer offers you a parachute, allowing you to quickly reach Gear Island.

    As you descend, your bird's-eye view of Gear Island reveals why you had trouble finding anyone
    on your way up: half of Gear Island is empty, but the half below you is a giant factory city!

    You land near the gradually-filling pool of lava at the base of your new lavafall. Lavaducts
    will eventually carry the lava throughout the city, but to make use of it immediately, Elves are
    loading it into large crucibles on wheels.

    The crucibles are top-heavy and pushed by hand. Unfortunately, the crucibles become very
    difficult to steer at high speeds, and so it can be hard to go in a straight line for very
    long.

    To get Desert Island the machine parts it needs as soon as possible, you'll need to find the
    best way to get the crucible from the lava pool to the machine parts factory. To do this, you
    need to minimize heat loss while choosing a route that doesn't require the crucible to go in a
    straight line for too long.

    Fortunately, the Elves here have a map (your puzzle input) that uses traffic patterns, ambient
    temperature, and hundreds of other parameters to calculate exactly how much heat loss can be
    expected for a crucible entering any particular city block.

    .. code-block:: text
        :caption: For example

        2413432311323
        3215453535623
        3255245654254
        3446585845452
        4546657867536
        1438598798454
        4457876987766
        3637877979653
        4654967986887
        4564679986453
        1224686865563
        2546548887735
        4322674655533

    Each city block is marked by a single digit that represents the amount of heat loss if the
    crucible enters that block. The starting point, the lava pool, is the top-left city block; the
    destination, the machine parts factory, is the bottom-right city block. (Because you already
    start in the top-left block, you don't incur that block's heat loss unless you leave that block
    and then return to it.)

    Because it is difficult to keep the top-heavy crucible going in a straight line for very long,
    it can move at most three blocks in a single direction before it must turn 90 degrees left or
    right. The crucible also can't reverse direction; after entering each city block, it may only
    turn left, continue straight, or turn right.

    .. code-block:: text
        :caption: One way to minimize heat loss is this path

        2>>34^>>>1323
        32v>>>35v5623
        32552456v>>54
        3446585845v52
        4546657867v>6
        14385987984v4
        44578769877v6
        36378779796v>
        465496798688v
        456467998645v
        12246868655<v
        25465488877v5
        43226746555v>
    """
    year = 2023
    day = 17

    def parse_data(self, data):
        data = data.splitlines()
        end = (len(data[0]) - 1, len(data) - 1)
        return end, {(x, y): int(data[y][x]) for x in range(len(data[0])) for y in range(len(data))}

        return np.array([list(line) for line in chunk.splitlines()])

    def find_end(self, min_chain: int, max_chain: int, loss_offset: int = 0) -> int:
        heapify(queue := [(0, 1, 0, (0, 0))])
        best = {}
        end, grid = self.data
        adj = lambda x, y: ((x + 1, y), (x, y + 1), (x - 1, y), (x, y - 1))

        while queue:
            loss, chain, direction, current = heappop(queue)
            if (key := (current, direction, chain)) in best and best[key] <= loss:
                continue
            if current == end and chain >= min_chain:
                return loss - loss_offset
            best[key] = loss
            neighbours = adj(*current)
            for e, d in enumerate([(direction - 1) % 4, direction, (direction + 1) % 4]):
                if [chain < min_chain, chain == max_chain][e % 2]:
                    continue
                next_chain = [1, chain + 1][e % 2]
                if (neighbour := neighbours[d]) in grid and best.get((neighbour, d, next_chain), loss + 1) > loss:
                    heappush(queue, (loss + grid[neighbour], next_chain, d, neighbour))

    async def solution_1(self):
        """
        This path never moves more than three consecutive blocks in the same direction and incurs a heat
        loss of only 102.

        Directing the crucible from the lava pool to the machine parts factory, but not moving more than
        three consecutive blocks in the same direction, what is the least heat loss it can incur?

        .. runcmd:: poetry run aoc --year 2023 run -d17 -s1 --real
            :caption: Part 1 Solution
        """
        return self.find_end(0, 3)

    async def solution_2(self):
        """
        The crucibles of lava simply aren't large enough to provide an adequate supply of lava to
        the machine parts factory. Instead, the Elves are going to upgrade to ultra crucibles.

        Ultra crucibles are even more difficult to steer than normal crucibles. Not only do they
        have trouble going in a straight line, but they also have trouble turning!

        Once an ultra crucible starts moving in a direction, it needs to move a minimum of four
        blocks in that direction before it can turn (or even before it can stop at the end).
        However, it will eventually start to get wobbly: an ultra crucible can move a maximum of ten
        consecutive blocks without turning.

        In the above example, an ultra crucible could follow this path to minimize heat loss:

        .. code-block:: text
            2>>>>>>>>1323
            32154535v5623
            32552456v4254
            34465858v5452
            45466578v>>>>
            143859879845v
            445787698776v
            363787797965v
            465496798688v
            456467998645v
            122468686556v
            254654888773v
            432267465553v

        In the above example, an ultra crucible would incur the minimum possible heat loss of 94.

        Here's another example:

            .. code-block:: text

            111111111111
            999999999991
            999999999991
            999999999991
            999999999991

        Sadly, an ultra crucible would need to take an unfortunate path like this one:

            .. code-block:: text

            1>>>>>>>1111
            9999999v9991
            9999999v9991
            9999999v9991
            9999999v>>>>

        This route causes the ultra crucible to incur the minimum possible heat loss of 71.

        Directing the ultra crucible from the lava pool to the machine parts factory, what is the
        least heat loss it can incur?

        .. runcmd:: poetry run aoc --year 2023 run -d17 -s2 --real
            :caption: Part 2 Solution
        """
        # Not quite sure why I needed the offset ONLY for my puzzle input
        return self.find_end(4, 10, loss_offset=0 if self.use_sample else 4)
