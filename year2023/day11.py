from aoc import AdventOfCode



class AOCYear2023Day11(AdventOfCode):
    """Day 11: Cosmic Expansion
    You continue following signs for "Hot Springs" and eventually come across an observatory. The Elf within turns out to be a researcher studying cosmic expansion using the giant telescope here.

    He doesn't know anything about the missing machine parts; he's only visiting for this research project. However, he confirms that the hot springs are the next-closest area likely to have people; he'll even take you straight there once he's done with today's observation analysis.

    Maybe you can help him with the analysis to speed things up?

    The researcher has collected a bunch of data and compiled the data into a single giant image (your puzzle input). The image includes empty space (.) and galaxies (#). For example:

    .. code-block:: text

        ...#......
        .......#..
        #.........
        ..........
        ......#...
        .#........
        .........#
        ..........
        .......#..
        #...#.....

    The researcher is trying to figure out the sum of the lengths of the shortest path between every pair of galaxies. However, there's a catch: the universe expanded in the time it took the light from those galaxies to reach the observatory.

    Due to something involving gravitational effects, only some space expands. In fact, the result is that any rows or columns that contain no galaxies should all actually be twice as big.

    In the above example, three columns and two rows contain no galaxies:

    .. code-block:: text

           v  v  v
         ...#......
         .......#..
         #.........
        >..........<
         ......#...
         .#........
         .........#
        >..........<
         .......#..
         #...#.....
           ^  ^  ^

    These rows and columns need to be twice as big; the result of cosmic expansion therefore looks like this:

    .. code-block:: text

        ....#........
        .........#...
        #............
        .............
        .............
        ........#....
        .#...........
        ............#
        .............
        .............
        .........#...
        #....#.......

    Equipped with this expanded universe, the shortest path between every pair of galaxies can be found. It can help to assign every galaxy a unique number:

    .. code-block:: text

        ....1........
        .........2...
        3............
        .............
        .............
        ........4....
        .5...........
        ............6
        .............
        .............
        .........7...
        8....9.......

    In these 9 galaxies, there are 36 pairs. Only count each pair once; order within the pair doesn't matter. For each pair, find any shortest path between the two galaxies using only steps that move up, down, left, or right exactly one . or # at a time. (The shortest path between two galaxies is allowed to pass through another galaxy.)

    For example, here is one of the shortest paths between galaxies 5 and 9:

    .. code-block:: text

        ....1........
        .........2...
        3............
        .............
        .............
        ........4....
        .5...........
        .##.........6
        ..##.........
        ...##........
        ....##...7...
        8....9.......

    This path has length 9 because it takes a minimum of nine steps to get from galaxy 5 to galaxy 9 (the eight locations marked # plus the step onto galaxy 9 itself). Here are some other example shortest path lengths:

    - Between galaxy 1 and galaxy 7: 15
    - Between galaxy 3 and galaxy 6: 17
    - Between galaxy 8 and galaxy 9: 5

    In this example, after expanding the universe, the sum of the shortest path between all 36 pairs of galaxies is 374.
    """
    year = 2023
    day = 11

    def parse_data(self, data):
        data = [list(row) for row in data.splitlines()]

        empty_rows = [i for i, row in enumerate(data) if set(row) == {"."}]
        empty_cols = [i for i, col in enumerate(zip(*data)) if set(col) == {"."}]

        galaxy_coords = []

        for y, row in enumerate(data):
            galaxy_coords.extend((x, y) for x, col in enumerate(row) if col == "#")

        return galaxy_coords, empty_rows, empty_cols

    def _between(self, a: int, val: int, b: int) -> bool:
        if a > b:
            a, b = b, a
        return a < val < b

    def _distance(self, a, b, empty_rows, empty_cols, expansion_factor=1):
        x1, y1 = a
        x2, y2 = b
        c = 0
        for row in empty_rows:
            if self._between(y1, row, y2):
                c += 1
        for col in empty_cols:
            if self._between(x1, col, x2):
                c += 1
        expansion_factor -= 1
        return (abs(x1 - x2) + abs(y1 - y2)) + c * expansion_factor

    def _distance_sum(self, expansion_factor):
        galaxies, empty_rows, empty_cols = self.data

        distances = []
        for i, coords in enumerate(galaxies[:-1]):
            distances.extend([
                self._distance(coords, other_coords, empty_rows, empty_cols, expansion_factor)
                for other_coords in galaxies[i + 1:]
            ])

        return sum(distances)

    async def solution_1(self):
        """
        Expand the universe, then find the length of the shortest path between every pair of galaxies. What is the sum of these lengths?

        .. runcmd:: poetry run aoc --year 2023 run -d11 -s1 --real
            :caption: Part 1 Solution
        """
        return self._distance_sum(2)

    async def solution_2(self):
        """
        The galaxies are much older (and thus much farther apart) than the researcher initially estimated.

        Now, instead of the expansion you did before, make each empty row or column one million times larger. That is, each empty row should be replaced with 1000000 empty rows, and each empty column should be replaced with 1000000 empty columns.

        (In the example above, if each empty row or column were merely 10 times larger, the sum of the shortest paths between every pair of galaxies would be 1030. If each empty row or column were merely 100 times larger, the sum of the shortest paths between every pair of galaxies would be 8410. However, your universe will need to expand far beyond these values.)

        Starting with the same initial image, expand the universe according to these new rules, then find the length of the shortest path between every pair of galaxies. What is the sum of these lengths?

        .. runcmd:: poetry run aoc --year 2023 run -d11 -s2 --real
            :caption: Part 2 Solution
        """
        return self._distance_sum(1000000)
