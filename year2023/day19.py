from __future__ import annotations

from collections import defaultdict
from copy import deepcopy
from functools import lru_cache
import itertools
import math
import re
import typing as t

from aoc import AdventOfCode



class Rule:
    def __init__(self, condition, comparator, value, workflow):
        self.condition = condition
        self.comparator = comparator
        self.value = int(value)
        self.target_workflow = workflow

    def __repr__(self):
        return f"Rule({self.condition}{self.comparator}{self.value}:{self.target_workflow})"

    def __eq__(self, other):
        if isinstance(other, Rule):
            return (
                self.condition == other.condition
                and self.comparator == other.comparator
                and self.value == other.value
                and self.target_workflow == other.target_workflow
            )

        if not isinstance(other, dict):
            raise NotImplementedError(f"Cannot compare Rule to {type(other)}")

        try:
            val = other.get(self.condition)
        except AttributeError:
            raise NotImplementedError(f"Cannot compare Rule to {type(other)}") from None

        if self.comparator == "<":
            return val < self.value
        elif self.comparator == ">":
            return val > self.value

        return False

    def get_ranges(self, ranges: t.Dict[str, range]):
        if self.comparator == "<":
            branch_true = range(ranges[self.condition].start, self.value)
            branch_false = range(self.value, ranges[self.condition].stop)
            range_true = deepcopy(ranges)
            range_true[self.condition] = branch_true

            ranges[self.condition] = branch_false
            return range_true
        else:
            branch_true = range(self.value + 1, ranges[self.condition].stop)
            branch_false = range(ranges[self.condition].start, self.value + 1)
            range_true = deepcopy(ranges)
            range_true[self.condition] = branch_true

            ranges[self.condition] = branch_false
            return range_true


ACCEPTED = "A"
REJECTED = "R"


class Workflow:
    _registry: t.Dict[str, Workflow] = {}

    def __init__(self, name: str, rules: t.List[Rule], otherwise: str):
        self.name = name
        self.rules = rules
        self.otherwise = otherwise
        self.register(name, self)

    def __repr__(self):
        return f"Workflow({self.name}, {self.rules})"

    @classmethod
    def get(cls, name: str) -> Workflow:
        return cls._registry[name]

    @classmethod
    def register(cls, name, workflow: Workflow):
        cls._registry[name] = workflow

    def run(self, part: t.Dict[str, int]) -> t.Optional[t.Union[bool, Workflow]]:
        for rule in self.rules:
            if rule == part:
                if rule.target_workflow == REJECTED:
                    return None
                if rule.target_workflow == ACCEPTED:
                    return True
                return self._registry[rule.target_workflow]

        if self.otherwise == REJECTED:
            return None
        if self.otherwise == ACCEPTED:
            return True
        return self._registry[self.otherwise]

    @classmethod
    def accepted_combinations(cls, ranges: t.Dict[str, range], target: str = "in") -> int:
        """
        Return the number of combinations of ratings that will be accepted by this workflow.
        """
        if target == ACCEPTED:
            return math.prod(len(range) for range in ranges.values())
        if target == REJECTED:
            return 0

        wf = cls.get(target)
        count = 0

        for rule in wf.rules:
            range_true = rule.get_ranges(ranges)
            count += cls.accepted_combinations(range_true, rule.target_workflow)

        return count + cls.accepted_combinations(ranges, wf.otherwise)


class AOCYear2023Day19(AdventOfCode):
    """
    Day 19: Aplenty

    The Elves of Gear Island are thankful for your help and send you on your way. They even have a
    hang glider that someone stole from Desert Island; since you're already going that direction, it
    would help them a lot if you would use it to get down there and return it to them.

    As you reach the bottom of the relentless avalanche of machine parts, you discover that they're
    already forming a formidable heap. Don't worry, though - a group of Elves is already here
    organizing the parts, and they have a system.

    To start, each part is rated in each of four categories:

    - ``x ``: Extremely cool looking
    - ``m ``: Musical (it makes a noise when you hit it)
    - ``a ``: Aerodynamic
    - ``s ``: Shiny

    Then, each part is sent through a series of workflows that will ultimately accept or reject the
    part. Each workflow has a name and contains a list of rules; each rule specifies a condition and
    where to send the part if the condition is true. The first rule that matches the part being
    considered is applied immediately, and the part moves on to the destination described by the
    rule. (The last rule in each workflow has no condition and always applies if reached.)

    Consider the workflow ex{x>10:one,m<20:two,a>30:R,A}. This workflow is named ex and contains
    four rules. If workflow ex were considering a specific part, it would perform the following
    steps in order:

    - Rule "x>10:one": If the part's x is more than 10, send the part to the workflow named one.
    - Rule "m<20:two": Otherwise, if the part's m is less than 20, send the part to the workflow
      named two.
    - Rule "a>30:R": Otherwise, if the part's a is more than 30, the part is immediately rejected
      (R).
    - Rule "A": Otherwise, because no other rules matched the part, the part is immediately accepted
      (A).

    If a part is sent to another workflow, it immediately switches to the start of that workflow
    instead and never returns. If a part is accepted (sent to A) or rejected (sent to R), the part
    immediately stops any further processing.

    The system works, but it's not keeping up with the torrent of weird metal shapes. The Elves ask if you can help sort a few parts and give you the list of workflows and some part ratings (your puzzle input). For example:

    .. code-block:: text

        px{a<2006:qkq,m>2090:A,rfg}
        pv{a>1716:R,A}
        lnx{m>1548:A,A}
        rfg{s<537:gd,x>2440:R,A}
        qs{s>3448:A,lnx}
        qkq{x<1416:A,crn}
        crn{x>2662:A,R}
        in{s<1351:px,qqz}
        qqz{s>2770:qs,m<1801:hdj,R}
        gd{a>3333:R,R}
        hdj{m>838:A,pv}

        {x=787,m=2655,a=1222,s=2876}
        {x=1679,m=44,a=2067,s=496}
        {x=2036,m=264,a=79,s=2244}
        {x=2461,m=1339,a=466,s=291}
        {x=2127,m=1623,a=2188,s=1013}

    The workflows are listed first, followed by a blank line, then the ratings of the parts the
    Elves would like you to sort. All parts begin in the workflow named in. In this example, the
    five listed parts go through the following workflows:

    - {x=787,m=2655,a=1222,s=2876}: in -> qqz -> qs -> lnx -> A
    - {x=1679,m=44,a=2067,s=496}: in -> px -> rfg -> gd -> R
    - {x=2036,m=264,a=79,s=2244}: in -> qqz -> hdj -> pv -> A
    - {x=2461,m=1339,a=466,s=291}: in -> px -> qkq -> crn -> R
    - {x=2127,m=1623,a=2188,s=1013}: in -> px -> rfg -> A

    Ultimately, three parts are accepted. Adding up the x, m, a, and s rating for each of the
    accepted parts gives 7540 for the part with x=787, 4623 for the part with x=2036, and 6951 for
    the part with x=2127. Adding all of the ratings for all of the accepted parts gives the sum
    total of 19114.
    """
    year = 2023
    day = 19

    def parse_data(self, data):
        workflow_re = re.compile(r"^(?P<name>[a-z]+){(?P<rules>.*)}$")
        rule_re = re.compile(r"(?P<condition>[a-z])(?P<comparator>[><])(?P<value>\d+):(?P<workflow>[a-zAR]+)")
        part_re = re.compile(r"(?P<param>\w)=(?P<value>\d+)")
        raw_workflows, raw_parts = data.split("\n\n")

        parts = []

        for line in raw_workflows.splitlines():
            match = workflow_re.match(line)
            if not match:
                raise ValueError(f"Invalid workflow: {line}")

            name = match.group("name")
            raw_rules = match.group("rules").split(",")
            rules = []
            default = None
            for rule in raw_rules:
                match = rule_re.match(rule)
                if not match:
                    default = rule
                    break
                rules.append(Rule(
                    condition=match.group("condition"),
                    comparator=match.group("comparator"),
                    value=int(match.group("value")),
                    workflow=match.group("workflow"),
                ))
            Workflow(name, rules, default)

        for line in raw_parts.splitlines():
            parts.append(
                {
                    match.group("param"): int(match.group("value"))
                    for match in part_re.finditer(line)
                }
            )

        return parts

    def check_parts(self, parts: t.List[t.Dict[str, int]]):
        accepted = []

        for part in parts:
            cur_workflow = Workflow.get("in")
            while True:
                workflow = cur_workflow.run(part)
                if workflow is None:
                    break
                if workflow is True:
                    accepted.append(part)
                    break
                cur_workflow = workflow
        return accepted

    async def solution_1(self):
        """
        Sort through all of the parts you've been given; what do you get if you add together all of the
        rating numbers for all of the parts that ultimately get accepted?

        .. runcmd:: poetry run aoc --year 2023 run -d19 -s1 --real
            :caption: Part 1 Solution
        """
        return sum(sum(part.values()) for part in self.check_parts(self.data))

    async def solution_2(self):
        """
        Even with your help, the sorting process still isn't fast enough.

        One of the Elves comes up with a new plan: rather than sort parts individually through all
        of these workflows, maybe you can figure out in advance which combinations of ratings will
        be accepted or rejected.

        Each of the four ratings (x, m, a, s) can have an integer value ranging from a minimum of 1
        to a maximum of 4000. Of all possible distinct combinations of ratings, your job is to
        figure out which ones will be accepted.

        In the above example, there are 167409079868000 distinct combinations of ratings that will
        be accepted.

        Consider only your list of workflows; the list of part ratings that the Elves wanted you to
        sort is no longer relevant. How many distinct combinations of ratings will be accepted by
        the Elves' workflows?

        .. runcmd:: poetry run aoc --year 2023 run -d19 -s2 --real
            :caption: Part 2 Solution
        """
        _ = self.data

        ranges = {
            "x": range(1, 4001),
            "m": range(1, 4001),
            "a": range(1, 4001),
            "s": range(1, 4001),
        }
        return Workflow.accepted_combinations(ranges)
