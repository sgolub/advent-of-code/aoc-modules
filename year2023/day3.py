import itertools
import math
import re

from aoc import AdventOfCode



class AOCYear2023Day3(AdventOfCode):
    """Day 3: Gear Ratios ---
    You and the Elf eventually reach a gondola lift station; he says the gondola lift will take you up to the water source, but this is as far as he can bring you. You go inside.

    It doesn't take long to find the gondolas, but there seems to be a problem: they're not moving.

    "Aaah!"

    You turn around to see a slightly-greasy Elf with a wrench and a look of surprise. "Sorry, I wasn't expecting anyone! The gondola lift isn't working right now; it'll still be a while before I can fix it." You offer to help.

    The engineer explains that an engine part seems to be missing from the engine, but nobody can figure out which one. If you can add up all the part numbers in the engine schematic, it should be easy to work out which part is missing.

    """
    year = 2023
    day = 3

    _regex_numbers = re.compile(r"(\d+)")
    _regex_symbols = re.compile(r"([^\.\d\s])")

    def parse_data(self, data):
        return data.splitlines()

    def _find_numbers(self, line):
        return [m for m in self._regex_numbers.finditer(line)]

    def _find_symbols(self, line):
        return [m for m in self._regex_symbols.finditer(line)]

    def _is_adjacent(self, match: re.Match, line_idx: int, sym_loc_list):
        x_range = list(range(match.start(), match.end()))
        match_coords = [(x, line_idx) for x in x_range]

        for x1, y1 in match_coords:
            for x2, y2 in sym_loc_list:
                if self._points_adjacent((x1, y1), (x2, y2)):
                    return True

        return False

    def _distance_between_points(self, p1, p2):
        return math.sqrt((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2)

    def _points_adjacent(self, p1, p2):
        return self._distance_between_points(p1, p2) <= 1.5

    def _get_num_locs(self):
        return [self._find_numbers(d) for d in self.data]

    def _get_symbol_grid(self):
        return [list(self._find_symbols(d)) for d in self.data]

    def _get_sym_locs(self):
        return set(itertools.chain(*[[(m.start(), y) for m in row] for y, row in
            enumerate(self._get_symbol_grid())]))

    async def solution_1(self):
        """
        The engine schematic (your puzzle input) consists of a visual representation of the engine. There are lots of numbers and symbols you don't really understand, but apparently any number adjacent to a symbol, even diagonally, is a "part number" and should be included in your sum. (Periods (.) do not count as a symbol.)

        Here is an example engine schematic:

        .. code-block:: text

            467..114..
            ...*......
            ..35..633.
            ......#...
            617*......
            .....+.58.
            ..592.....
            ......755.
            ...$.*....
            .664.598..

        In this schematic, two numbers are not part numbers because they are not adjacent to a symbol: 114 (top right) and 58 (middle right). Every other number is adjacent to a symbol and so is a part number; their sum is 4361.

        Of course, the actual engine schematic is much larger. What is the sum of all of the part
        numbers in the engine schematic?

        .. runcmd:: poetry run aoc --year 2023 run -d7 -s1 --real
            :caption: Part 1 Solution
        """
        num_loc_list = self._get_num_locs()
        sym_loc_list = self._get_sym_locs()

        next_to_symbols = []
        for line_idx, loc in enumerate(num_loc_list):
            for num_match in loc:
                if self._is_adjacent(num_match, line_idx, sym_loc_list):
                    next_to_symbols.append(int(num_match.group(1)))
        return sum(next_to_symbols)

    async def solution_2(self):
        """The engineer finds the missing part and installs it in the engine! As the engine springs to life, you jump in the closest gondola, finally ready to ascend to the water source.

        You don't seem to be going very fast, though. Maybe something is still wrong? Fortunately, the gondola has a phone labeled "help", so you pick it up and the engineer answers.

        Before you can explain the situation, she suggests that you look out the window. There stands the engineer, holding a phone in one hand and waving with the other. You're going so slowly that you haven't even left the station. You exit the gondola.

        The missing part wasn't the only issue - one of the gears in the engine is wrong. A gear is any * symbol that is adjacent to exactly two part numbers. Its gear ratio is the result of multiplying those two numbers together.

        This time, you need to find the gear ratio of every gear and add them all up so that the engineer can figure out which gear needs to be replaced.

        Consider the same engine schematic again:

        .. code-block:: text

            467..114..
            ...*......
            ..35..633.
            ......#...
            617*......
            .....+.58.
            ..592.....
            ......755.
            ...$.*....
            .664.598..

        In this schematic, there are two gears. The first is in the top left; it has part numbers 467 and 35, so its gear ratio is 16345. The second gear is in the lower right; its gear ratio is 451490. (The * adjacent to 617 is not a gear because it is only adjacent to one part number.) Adding up all of the gear ratios produces 467835.

        What is the sum of all of the gear ratios in your engine schematic?

        .. runcmd:: poetry run aoc --year 2023 run -d7 -s2 --real
            :caption: Part 2 Solution
        """
        num_loc_list = self._get_num_locs()
        star_locations = set(itertools.chain(*[[(m.start(), y) for m in row if m.groups(1)[0] == "*"] for y, row in enumerate(self._get_symbol_grid())]))

        gear_ratio = []
        for star_loc in star_locations:
            adjacent_nums = []
            for line_idx, loc in enumerate(num_loc_list):
                for num_match in loc:
                    if self._is_adjacent(num_match, line_idx, [star_loc]):
                        adjacent_nums.append(int(num_match.group(1)))
            if len(adjacent_nums) == 2:
                gear_ratio.append(math.prod(adjacent_nums))
        return sum(gear_ratio)
