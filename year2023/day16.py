from collections import deque
from aoc import AdventOfCode



class AOCYear2023Day16(AdventOfCode):
    r"""Day 16: The Floor Will Be Lava

    With the beam of light completely focused somewhere, the reindeer leads you deeper still into
    the Lava Production Facility. At some point, you realize that the steel facility walls have been
    replaced with cave, and the doorways are just cave, and the floor is cave, and you're pretty
    sure this is actually just a giant cave.

    Finally, as you approach what must be the heart of the mountain, you see a bright light in a
    cavern up ahead. There, you discover that the beam of light you so carefully focused is emerging
    from the cavern wall closest to the facility and pouring all of its energy into a contraption on
    the opposite side.

    Upon closer inspection, the contraption appears to be a flat, two-dimensional square grid
    containing empty space (.), mirrors (/ and \), and splitters (| and -).

    The contraption is aligned so that most of the beam bounces around the grid, but each tile on
    the grid converts some of the beam's light into heat to melt the rock in the cavern.

    You note the layout of the contraption (your puzzle input). For example:

    .. code-block:: text

        .|...\....
        |.-.\.....
        .....|-...
        ........|.
        ..........
        .........\
        ..../.\\..
        .-.-/..|..
        .|....-|.\
        ..//.|....

    The beam enters in the top-left corner from the left and heading to the right. Then, its
    behavior depends on what it encounters as it moves:

    - If the beam encounters empty space (.), it continues in the same direction.
    - If the beam encounters a mirror (/ or \), the beam is reflected 90 degrees depending on the
      angle of the mirror. For instance, a rightward-moving beam that encounters a / mirror would
      continue upward in the mirror's column, while a rightward-moving beam that encounters a \
      mirror would continue downward from the mirror's column.
    - If the beam encounters the pointy end of a splitter (| or -), the beam passes through the
      splitter as if the splitter were empty space. For instance, a rightward-moving beam that
      encounters a - splitter would continue in the same direction.
    - If the beam encounters the flat side of a splitter (| or -), the beam is split into two beams
      going in each of the two directions the splitter's pointy ends are pointing. For instance, a
      rightward-moving beam that encounters a | splitter would split into two beams: one that
      continues upward from the splitter's column and one that continues downward from the
      splitter's column.

    Beams do not interact with other beams; a tile can have many beams passing through it at the
    same time. A tile is energized if that tile has at least one beam pass through it, reflect in
    it, or split in it.

    In the above example, here is how the beam of light bounces around the contraption:

    .. code-block:: text

        >|<<<\....
        |v-.\^....
        .v...|->>>
        .v...v^.|.
        .v...v^...
        .v...v^..\
        .v../2\\..
        <->-/vv|..
        .|<<<2-|.\
        .v//.|.v..

    Beams are only shown on empty tiles; arrows indicate the direction of the beams. If a tile
    contains beams moving in multiple directions, the number of distinct directions is shown
    instead. Here is the same diagram but instead only showing whether a tile is energized (#) or
    not (.):

    .. code-block:: text

        ######....
        .#...#....
        .#...#####
        .#...##...
        .#...##...
        .#...##...
        .#..####..
        ########..
        .#######..
        .#...#.#..

    Ultimately, in this example, 46 tiles become energized.
    """
    year = 2023
    day = 16

    def parse_data(self, data):
        return data.splitlines()

    def solve(self, y, x, change_y, change_x):
        max_y, max_x = len(self.data), len(self.data[0])
        q = deque([(y, x, change_y, change_x)])
        seen = set()
        while q:
            y, x, change_y, change_x = q.popleft()
            if 0 > y or y >= max_y or 0 > x or x >= max_x or (y, x, change_y, change_x) in seen:
                continue
            seen.add((y, x, change_y, change_x))
            match self.data[y][x]:
                case "/":
                    q.append((y - change_x, x - change_y, -change_x, -change_y))
                case "\\":
                    q.append((y + change_x, x + change_y, change_x, change_y))
                case "|" if change_x:
                    q.append((y + 1, x, 1, 0))
                    q.append((y - 1, x, -1, 0))
                case "-" if change_y:
                    q.append((y, x + 1, 0, 1))
                    q.append((y, x - 1, 0, -1))
                case _:
                    q.append((y + change_y, x + change_x, change_y, change_x))
        return len(set((y, x) for y, x, _, _ in seen))

    async def solution_1(self):
        """
        The light isn't energizing enough tiles to produce lava; to debug the contraption, you need
        to start by analyzing the current situation. With the beam starting in the top-left heading
        right, how many tiles end up being energized?

        .. runcmd:: poetry run aoc --year 2023 run -d16 -s1 --real
            :caption: Part 1 Solution
        """
        return self.solve(0, 0, 0, 1)

    async def solution_2(self):
        r"""
        As you try to work out what might be wrong, the reindeer tugs on your shirt and leads you to
        a nearby control panel. There, a collection of buttons lets you align the contraption so
        that the beam enters from any edge tile and heading away from that edge. (You can choose
        either of two directions for the beam if it starts on a corner; for instance, if the beam
        starts in the bottom-right corner, it can start heading either left or upward.)

        So, the beam could start on any tile in the top row (heading downward), any tile in the
        bottom row (heading upward), any tile in the leftmost column (heading right), or any tile in
        the rightmost column (heading left). To produce lava, you need to find the configuration
        that energizes as many tiles as possible.

        In the above example, this can be achieved by starting the beam in the fourth tile from the
        left in the top row:

        .. code-block:: text

            .|<2<\....
            |v-v\^....
            .v.v.|->>>
            .v.v.v^.|.
            .v.v.v^...
            .v.v.v^..\
            .v.v/2\\..
            <-2-/vv|..
            .|<<<2-|.\
            .v//.|.v..

        Using this configuration, 51 tiles are energized:

        .. code-block:: text

            .#####....
            .#.#.#....
            .#.#.#####
            .#.#.##...
            .#.#.##...
            .#.#.##...
            .#.#####..
            ########..
            .#######..
            .#...#.#..

        Find the initial beam configuration that energizes the largest number of tiles; how many
        tiles are energized in that configuration?

        .. runcmd:: poetry run aoc --year 2023 run -d16 -s2 --real
            :caption: Part 2 Solution
        """
        max_y, max_x = len(self.data), len(self.data[0])
        max_energy = 0
        for i, j, di, dj in (
            [(x, 0, 0, 1) for x in range(max_y)]
            + [(x, max_x - 1, 0, -1) for x in range(max_y)]
            + [(0, x, 1, 0) for x in range(max_x)]
            + [(max_y - 1, x, -1, 0) for x in range(max_x)]
        ):
            max_energy = max(max_energy, self.solve(i, j, di, dj))
        return max_energy
