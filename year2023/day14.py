from aoc import AdventOfCode
import numpy as np


class AOCYear2023Day14(AdventOfCode):
    """Parabolic Reflector Dish ---

    You reach the place where all of the mirrors were pointing: a massive parabolic reflector dish
    attached to the side of another large mountain.

    The dish is made up of many small mirrors, but while the mirrors themselves are roughly in the
    shape of a parabolic reflector dish, each individual mirror seems to be pointing in slightly the
    wrong direction. If the dish is meant to focus light, all it's doing right now is sending it in
    a vague direction.

    This system must be what provides the energy for the lava! If you focus the reflector dish,
    maybe you can go where it's pointing and use the light to fix the lava production.

    Upon closer inspection, the individual mirrors each appear to be connected via an elaborate
    system of ropes and pulleys to a large metal platform below the dish. The platform is covered in
    large rocks of various shapes. Depending on their position, the weight of the rocks deforms the
    platform, and the shape of the platform controls which ropes move and ultimately the focus of
    the dish.

    In short: if you move the rocks, you can focus the dish. The platform even has a control panel
    on the side that lets you tilt it in one of four directions! The rounded rocks (O) will roll
    when the platform is tilted, while the cube-shaped rocks (#) will stay in place. You note the
    positions of all of the empty spaces (.) and rocks (your puzzle input). For example:

    .. code-block:: text

        O....#....
        O.OO#....#
        .....##...
        OO.#O....O
        .O.....O#.
        O.#..O.#.#
        ..O..#O..O
        .......O..
        #....###..
        #OO..#....

    Start by tilting the lever so all of the rocks will slide north as far as they will go:

    .. code-block:: text

        OOOO.#.O..
        OO..#....#
        OO..O##..O
        O..#.OO...
        ........#.
        ..#....#.#
        ..O..#.O.O
        ..O.......
        #....###..
        #....#....

    You notice that the support beams along the north side of the platform are damaged; to ensure
    the platform doesn't collapse, you should calculate the total load on the north support beams.

    The amount of load caused by a single rounded rock (O) is equal to the number of rows from the
    rock to the south edge of the platform, including the row the rock is on. (Cube-shaped rocks (#)
    don't contribute to load.) So, the amount of load caused by each rock in each row is as
    follows:

    .. code-block:: text

        OOOO.#.O.. 10
        OO..#....#  9
        OO..O##..O  8
        O..#.OO...  7
        ........#.  6
        ..#....#.#  5
        ..O..#.O.O  4
        ..O.......  3
        #....###..  2
        #....#....  1

    The total load is the sum of the load caused by all of the rounded rocks. In this example, the
    total load is 136.
    """
    year = 2023
    day = 14

    def parse_data(self, data):
        return [np.array([list(line) for line in chunk.splitlines()]) for chunk in data.split("\n\n")]

    def _roll_stones_until_stopped_by_cubes(self, col: np.array):
        stones = np.where(col == "O")[0]
        cubes = np.where(col == "#")[0]
        if len(stones) == 0:
            return col
        if len(cubes) == 0:
            return np.array(['O'] * len(stones) + ['.'] * (len(col) - len(stones)))
        for i, stone in enumerate(np.where(stones < np.min(cubes))[0]):
            col[stones[stone]] = "."
            col[i] = "O"

        return np.array(col[:np.min(cubes)].tolist() + ["#"] + self._roll_stones_until_stopped_by_cubes(col[np.min(cubes) + 1:]).tolist())

    def _count_load(self, col: np.array):
        stones = np.where(col == "O")[0]
        return sum([len(col) - i for i in stones])

    def _step_cycle(self, pattern: np.array):
        return np.rot90(pattern, k=-1)

    def _roll_all_rows(self, pattern: np.array):
        flipped = np.rot90(pattern, k=1)
        return np.rot90([
            self._roll_stones_until_stopped_by_cubes(row)
            for row in flipped
        ], k=-1)

    def _calc_pattern_load(self, pattern: np.array):
        return sum([self._count_load(col) for col in np.rot90(pattern, k=1)])

    async def solution_1(self):
        """
        Tilt the platform so that the rounded rocks all roll north. Afterward, what is the total load on
        the north support beams?

        .. runcmd:: poetry run aoc --year 2023 run -d14 -s1 --real
            :caption: Part 1 Solution
        """
        return self._calc_pattern_load(self._roll_all_rows(self.data.copy()[0]))

    def _to_str(self, pattern: np.array):
        return "\n".join(["".join(row) for row in pattern])

    def _print_pattern(self, pattern: np.array, post: str = "\n\n"):
        print(self._to_str(pattern), end=post)

    def _spin_cyle(self, pattern: np.array):
        # roll north
        pattern = self._roll_all_rows(pattern)
        # roll west
        #  pattern = self._roll_all_rows(self._step_cycle(pattern))
        #  # roll south
        #  pattern = self._roll_all_rows(self._step_cycle(pattern))
        #  # roll east
        #  pattern = self._roll_all_rows(self._step_cycle(pattern))
        for _ in range(3):
            pattern = self._roll_all_rows(self._step_cycle(pattern))
        return self._step_cycle(pattern)

    async def solution_2(self):
        """
        The parabolic reflector dish deforms, but not in a way that focuses the beam. To do that,
        you'll need to move the rocks to the edges of the platform. Fortunately, a button on the
        side of the control panel labeled "spin cycle" attempts to do just that!

        Each cycle tilts the platform four times so that the rounded rocks roll north, then west,
        then south, then east. After each tilt, the rounded rocks roll as far as they can before the
        platform tilts in the next direction. After one cycle, the platform will have finished
        rolling the rounded rocks in those four directions in that order.

        Here's what happens in the example above after each of the first few cycles:

        .. code-block:: text
            :caption: After 1 cycle

            .....#....
            ....#...O#
            ...OO##...
            .OO#......
            .....OOO#.
            .O#...O#.#
            ....O#....
            ......OOOO
            #...O###..
            #..OO#....


        .. code-block:: text
            :caption: After 2 cycles

            .....#....
            ....#...O#
            .....##...
            ..O#......
            .....OOO#.
            .O#...O#.#
            ....O#...O
            .......OOO
            #..OO###..
            #.OOO#...O

        .. code-block:: text
            :caption: After 3 cycles

            .....#....
            ....#...O#
            .....##...
            ..O#......
            .....OOO#.
            .O#...O#.#
            ....O#...O
            .......OOO
            #...O###.O
            #.OOO#...O

        This process should work if you leave it running long enough, but you're still worried about
        the north support beams. To make sure they'll survive for a while, you need to calculate the
        total load on the north support beams after 1000000000 cycles.

        In the above example, after 1000000000 cycles, the total load on the north support beams is
        64.

        Run the spin cycle for 1000000000 cycles. Afterward, what is the total load on the north
        support beams?

        .. runcmd:: poetry run aoc --year 2023 run -d14 -s2 --real
            :caption: Part 2 Solution
        """

        pattern = self.data.copy()[0]
        cycle_count = 1_000_000_000
        seen = {}
        for i in range(cycle_count):
            seen[self._to_str(pattern)] = dict(
                i=i,
                load=self._calc_pattern_load(pattern),
            )
            pattern = self._spin_cyle(pattern)
            if self._to_str(pattern) in seen:
                prev_idx = seen[self._to_str(pattern)]['i']
                period = i + 1 - prev_idx
                remainder = (cycle_count - prev_idx) % period
                find_i = prev_idx + remainder
                for pattern_str, pattern_info in seen.items():
                    if pattern_info['i'] == find_i:
                        return pattern_info['load']
