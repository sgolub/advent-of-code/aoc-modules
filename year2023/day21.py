from aoc import AdventOfCode


class AOCYear2023Day21(AdventOfCode):
    year = 2023
    day = 21

    def parse_data(self, data):
        return data

    async def solution_1(self):
        """

        .. runcmd:: poetry run aoc --year 2023 run -d21 -s1 --real
            :caption: Part 1 Solution
        """
        raise NotImplementedError

    async def solution_2(self):
        """

        .. runcmd:: poetry run aoc --year 2023 run -d21 -s2 --real
            :caption: Part 2 Solution
        """
        raise NotImplementedError