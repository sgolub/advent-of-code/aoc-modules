from __future__ import annotations

from collections import defaultdict, deque
import math
import typing as t

from aoc import AdventOfCode


class Broadcaster:
    def __init__(self, modules, label, targetstr=""):
        self.label = label
        self.modules = modules
        self.targets = set(targetstr.split(", "))
        self.pulses = defaultdict(int)

    def add_targets(self, targetstr):
        self.targets.update(targetstr.split(", "))

    def receive(self, pulse):
        def callback():
            self.pulses[pulse] += 1
            for t in self.targets:
                yield self.modules[t].receive(self.label, pulse)

        callback.label = self.label
        callback.source = "none"
        callback.pulse = pulse
        return callback


class Output:
    def __init__(self):
        self.pulses = defaultdict(int)

    def receive(self, _, pulse):
        self.pulses[pulse] += 1


class FlipFlop:
    def __init__(self, modules, label, targetstr=""):
        self.label = label
        self.modules = modules
        self.targets = set(targetstr.split(", "))
        self.val = 0
        self.pulses = defaultdict(int)

    def add_targets(self, targetstr):
        self.targets.update(targetstr.split(", "))

    def receive(self, source, pulse):
        def callback():
            self.pulses[pulse] += 1
            if not pulse:
                self.val = 1 - self.val
                for t in self.targets:
                    yield self.modules[t].receive(self.label, self.val)

        callback.label = self.label
        callback.source = source
        callback.pulse = pulse
        return callback

    def __repr__(self):
        return f"< FlipFlop {self.label} >"


class Conjunction:
    def __init__(self, modules, label, targetstr=""):
        self.label = label
        self.modules = modules
        self.targets = set(targetstr.split(", "))
        self.inputs = {}
        self.pulses = defaultdict(int)

    def add_targets(self, targetstr):
        self.targets.update(targetstr.split(", "))

    def add_input(self, input):
        self.inputs[input] = 0

    def receive(self, source, pulse):
        def callback():
            self.pulses[pulse] += 1
            self.inputs[source] = pulse
            if all(x == 1 for x in self.inputs.values()):
                out = 0
            else:
                out = 1
            for t in self.targets:
                yield self.modules[t].receive(self.label, out)

        callback.label = self.label
        callback.source = source
        callback.pulse = pulse
        return callback

    def __repr__(self):
        return f"< Conjunction {self.label} >"


class AOCYear2023Day20(AdventOfCode):
    """Day 20: Pulse Propagation

    With your help, the Elves manage to find the right parts and fix all of the machines. Now, they
    just need to send the command to boot up the machines and get the sand flowing again.

    The machines are far apart and wired together with long cables. The cables don't connect to the
    machines directly, but rather to communication modules attached to the machines that perform
    various initialization tasks and also act as communication relays.

    Modules communicate using pulses. Each pulse is either a high pulse or a low pulse. When a
    module sends a pulse, it sends that type of pulse to each module in its list of destination
    modules.

    There are several different types of modules:

    ``Flip-flop`` modules (prefix %) are either on or off; they are initially off. If a flip-flop module receives a high pulse, it is ignored and nothing happens. However, if a flip-flop module receives a low pulse, it flips between on and off. If it was off, it turns on and sends a high pulse. If it was on, it turns off and sends a low pulse.

    ``Conjunction`` modules (prefix &) remember the type of the most recent pulse received from each of their connected input modules; they initially default to remembering a low pulse for each input. When a pulse is received, the conjunction module first updates its memory for that input. Then, if it remembers high pulses for all inputs, it sends a low pulse; otherwise, it sends a high pulse.

    There is a single broadcast module (named broadcaster). When it receives a pulse, it sends the same pulse to all of its destination modules.

    Here at Desert Machine Headquarters, there is a module with a single button on it called, aptly, the button module. When you push the button, a single low pulse is sent directly to the broadcaster module.

    After pushing the button, you must wait until all pulses have been delivered and fully handled before pushing it again. Never push the button if modules are still processing pulses.

    Pulses are always processed in the order they are sent. So, if a pulse is sent to modules a, b, and c, and then module a processes its pulse and sends more pulses, the pulses sent to modules b and c would have to be handled first.

    The module configuration (your puzzle input) lists each module. The name of the module is preceded by a symbol identifying its type, if any. The name is then followed by an arrow and a list of its destination modules. For example:

    broadcaster -> a, b, c
    %a -> b
    %b -> c
    %c -> inv
    &inv -> a
    In this module configuration, the broadcaster has three destination modules named a, b, and c. Each of these modules is a flip-flop module (as indicated by the % prefix). a outputs to b which outputs to c which outputs to another module named inv. inv is a conjunction module (as indicated by the & prefix) which, because it has only one input, acts like an inverter (it sends the opposite of the pulse type it receives); it outputs to a.

    By pushing the button once, the following pulses are sent:

    button -low-> broadcaster
    broadcaster -low-> a
    broadcaster -low-> b
    broadcaster -low-> c
    a -high-> b
    b -high-> c
    c -high-> inv
    inv -low-> a
    a -low-> b
    b -low-> c
    c -low-> inv
    inv -high-> a
    After this sequence, the flip-flop modules all end up off, so pushing the button again repeats the same sequence.

    Here's a more interesting example:

    broadcaster -> a
    %a -> inv, con
    &inv -> b
    %b -> con
    &con -> output
    This module configuration includes the broadcaster, two flip-flops (named a and b), a single-input conjunction module (inv), a multi-input conjunction module (con), and an untyped module named output (for testing purposes). The multi-input conjunction module con watches the two flip-flop modules and, if they're both on, sends a low pulse to the output module.

    Here's what happens if you push the button once:

    button -low-> broadcaster
    broadcaster -low-> a
    a -high-> inv
    a -high-> con
    inv -low-> b
    con -high-> output
    b -high-> con
    con -low-> output
    Both flip-flops turn on and a low pulse is sent to output! However, now that both flip-flops are on and con remembers a high pulse from each of its two inputs, pushing the button a second time does something different:

    button -low-> broadcaster
    broadcaster -low-> a
    a -low-> inv
    a -low-> con
    inv -high-> b
    con -high-> output
    Flip-flop a turns off! Now, con remembers a low pulse from module a, and so it sends only a high pulse to output.

    Push the button a third time:

    button -low-> broadcaster
    broadcaster -low-> a
    a -high-> inv
    a -high-> con
    inv -low-> b
    con -low-> output
    b -low-> con
    con -high-> output
    This time, flip-flop a turns on, then flip-flop b turns off. However, before b can turn off, the pulse sent to con is handled first, so it briefly remembers all high pulses for its inputs and sends a low pulse to output. After that, flip-flop b turns off, which causes con to update its state and send a high pulse to output.

    Finally, with a on and b off, push the button a fourth time:

    button -low-> broadcaster
    broadcaster -low-> a
    a -low-> inv
    a -low-> con
    inv -high-> b
    con -high-> output
    This completes the cycle: a turns off, causing con to remember only low pulses and restoring all modules to their original states.

    To get the cables warmed up, the Elves have pushed the button 1000 times. How many pulses got sent as a result (including the pulses sent by the button itself)?

    In the first example, the same thing happens every time the button is pushed: 8 low pulses and 4 high pulses are sent. So, after pushing the button 1000 times, 8000 low pulses and 4000 high pulses are sent. Multiplying these together gives 32000000.

    In the second example, after pushing the button 1000 times, 4250 low pulses and 2750 high pulses are sent. Multiplying these together gives 11687500.
    """
    year = 2023
    day = 20

    _state = defaultdict(int)
    _modules = {}
    _ff_modules = {}
    _queue = deque()

    def _flipflop(self, name, targets):
        state = 0
        def flipflop(_, pulse):
            nonlocal state
            if pulse == 0:
                state = not state
                for t in targets:
                    self._queue.append((state, name, t))
            self._ff_modules[name] = state
            return state
        self._modules[name] = flipflop
        return flipflop

    def _conjunction(self, name, targets):
        inputs = defaultdict(int)
        def conjunction(from_name, pulse):
            inputs[from_name] = pulse
            state = not any(inputs.values())
            for t in targets:
                self._queue.append((state, name, t))
            return state
        self._modules[name] = conjunction
        return conjunction

    def _broadcaster(self, targets):
        def broadcaster(pulse):
            for t in targets:
                self._queue.append((pulse, "broadcaster", t))
            return pulse
        self._modules["broadcaster"] = broadcaster
        return broadcaster

    def _parse_data(self, data):
        broadcaster = None
        for line in data.splitlines():
            raw_module, targets = line.split(" -> ")
            targets = [target.strip() for target in targets.split(", ")]
            if raw_module == "broadcaster":
                broadcaster = self._broadcaster(targets)
                continue
            if raw_module.startswith("%"):
                module = self._flipflop(raw_module[1:], targets)
                continue
            if raw_module.startswith("&"):
                module = self._conjunction(raw_module[1:], targets)
                continue

        return broadcaster

    def parse_data(self, data):
        modules = defaultdict(Output)
        pending_targets = defaultdict(list)
        conjunctions = set()
        for line in data.splitlines():
            modulestr, targetstr = line.split(" -> ")
            if modulestr == "broadcaster":
                modules["broadcaster"] = Broadcaster(modules, "broadcaster", targetstr)
                continue
            mtype, label = modulestr[0], modulestr[1:]
            if mtype == "%":
                modules[label] = FlipFlop(modules, label, targetstr)
            elif mtype == "&":
                modules[label] = Conjunction(modules, label, targetstr)
                conjunctions.add(label)
            for t in targetstr.split(", "):
                pending_targets[t].append(label)
        for conjlabel in conjunctions:
            conj = modules[conjlabel]
            for inp in pending_targets[conjlabel]:
                conj.add_input(inp)
        return modules

    def push_the_button(self, modules, times=1000):
        q = deque([])
        for _ in range(times):
            q.append(modules["broadcaster"].receive(0))
            while q:
                cb = q.popleft()
                if cb is not None:
                    q.extend(cb())

    async def solution_1(self):
        """
        Consult your module configuration; determine the number of low pulses and high pulses that would be sent after pushing the button 1000 times, waiting for all pulses to be fully handled after each push of the button. What do you get if you multiply the total number of low pulses sent by the total number of high pulses sent?

        .. runcmd:: poetry run aoc --year 2023 run -d20 -s1 --real
            :caption: Part 1 Solution
        """
        modules = self.data

        self.push_the_button(modules)
        return sum(m.pulses[0] for m in modules.values()) * sum(m.pulses[1] for m in modules.values())

    async def solution_2(self):
        """
        The final machine responsible for moving the sand down to Island Island has a module
        attached named rx. The machine turns on when a single low pulse is sent to rx.

        Reset all modules to their default states. Waiting for all pulses to be fully handled after
        each button press, what is the fewest number of button presses required to deliver a single
        low pulse to the module named rx?

        .. runcmd:: poetry run aoc --year 2023 run -d20 -s2 --real
            :caption: Part 2 Solution
        """

        modules = self.data
        L = []
        for i in range(1, 100_000):
            q = deque({})
            q.append(modules["broadcaster"].receive(0))
            while q:
                cb = q.popleft()
                if cb is not None:
                    if cb.label == "zr" and cb.pulse == 1:
                        L.append((i, cb.source))
                    q.extend(cb())

        return math.prod([x[0] for x in L[:4]])
