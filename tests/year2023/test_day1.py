from aoc.utils import get_day
import pytest



class TestDay1:
    @pytest.fixture
    def module(self):
        return get_day(2023, 1)

    @pytest.fixture(params=[True, False], ids=["sample", "real"])
    def use_sample(self, request):
        return request.param

    @pytest.fixture
    def day(self, module, use_sample):
        module.day.use_sample = use_sample
        return module.day

    def test_rreplace(self, module):
        assert module.rreplace("1abc2", "1", "2", 1) == "2abc2"
        assert module.rreplace("1abc2", "1", "2", 2) == "2abc2"
        assert module.rreplace("1abc2", "1", "2", 3) == "2abc2"
        assert module.rreplace("1abc2", "1", "2", 4) == "2abc2"
        assert module.rreplace("1abc2", "1", "2", 5) == "2abc2"
        assert module.rreplace("1abc2", "1", "2", 6) == "2abc2"
        assert module.rreplace("1abc2", "1", "2", 7) == "2abc2"
        assert module.rreplace("1abc2", "1", "2", 8) == "2abc2"
        assert module.rreplace("1abc2", "1", "2", 9) == "2abc2"

    def test_find_all(self, module):
        assert list(module.find_all("1abc2", "1")) == [0]
        assert list(module.find_all("1abc2", "2")) == [4]
        assert list(module.find_all("1abc2", "a")) == [1]
        assert list(module.find_all("1abc2", "b")) == [2]
        assert list(module.find_all("1abc2", "c")) == [3]
        assert list(module.find_all("1abc2", "d")) == []
        assert list(module.find_all("1abc2", "e")) == []
        assert list(module.find_all("1abc2", "f")) == []
        assert list(module.find_all("1abc2", "g")) == []
        assert list(module.find_all("1abc2", "h")) == []
        assert list(module.find_all("1abc2", "i")) == []
        assert list(module.find_all("1abc2", "j")) == []
        assert list(module.find_all("1abc2", "k")) == []
        assert list(module.find_all("1abc2", "l")) == []
        assert list(module.find_all("1abc2", "m")) == []

    def test_parse_line(self, day):
        assert day.parse_line("1abc2") == 12
        assert day.parse_line("pqr3stu8vwx") == 38
        assert day.parse_line("a1b2c3d4e5f") == 15
        assert day.parse_line("treb7uchet") == 77
        assert day.parse_line("6twofive3two") == 63

    def test_parse_line_with_spellings(self, day):
        assert day.parse_line_with_spellings("two1nine") == 29
        assert day.parse_line_with_spellings("eightwothree") == 83
        assert day.parse_line_with_spellings("abcone2threexyz") == 13
        assert day.parse_line_with_spellings("xtwone3four") == 24
        assert day.parse_line_with_spellings("4nineeightseven2") == 42
        assert day.parse_line_with_spellings("zoneight234") == 14
        assert day.parse_line_with_spellings("7pqrstsixteen") == 76
        assert day.parse_line_with_spellings("6twofive3two") == 62

    @pytest.fixture
    def solution_1_result(self, use_sample):
        return 142 if use_sample else 53194

    @pytest.fixture
    def solution_2_result(self, use_sample):
        return 281 if use_sample else 54249

    @pytest.mark.asyncio
    async def test_solution_1(self, day, solution_1_result):
        assert await day.solution_1() == solution_1_result

    @pytest.mark.asyncio
    async def test_solution_2(self, day, solution_2_result):
        assert await day.solution_2() == solution_2_result
